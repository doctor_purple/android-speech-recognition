package com.example.androidhttpgetservlet.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.androidhttpgetservlet.R;
import com.example.androidhttpgetservlet.enums.Commands;
import com.example.androidhttpgetservlet.fragments.DetailsFragment;
import com.example.androidhttpgetservlet.fragments.SolutionListFragment;
import com.example.androidhttpgetservlet.helpers.FilterHelper;
import com.example.androidhttpgetservlet.helpers.JSONHelper;
import com.example.androidhttpgetservlet.helpers.WordHelper;
import com.example.androidhttpgetservlet.interfaces.OnClickSolutionItemListener;
import com.example.androidhttpgetservlet.models.Solution;
import com.example.androidhttpgetservlet.models.Solution.OriginDestinationOptions;
import com.example.androidhttpgetservlet.services.SpeechRecognitionService;

/**
 * Created by dbattaglino on 29/08/13.
 * Display results in the new activity
 */
public class DisplayResultsActivity extends FragmentActivity implements OnClickSolutionItemListener, OnInitListener {

    
    //view elements
    TextView resumeInformationsFrom, resumeInformationsTo, resumeDate, requestDuration;
    ListView listSolutionsView;
    ImageButton cheapestImageButton, shortestImageButton, closestImageButton, arrivalTimeImageButton, reloadButton;
    
    //solutions data structure
    ArrayList<Solution> solutions = new ArrayList<Solution>();
    ArrayList<OriginDestinationOptions> originDestinationOptions;
    ArrayList<OriginDestinationOptions> tempOriginDestinationOptions;
    ArrayList<OriginDestinationOptions> originDestinationBestOnes = new ArrayList<OriginDestinationOptions>();
    Bundle bundle;
    OriginDestinationOptions selectedSolution;
    
    
    
    //results data structure
    HashMap<String, Object> results = new HashMap<String, Object>();
    String dateString;
    
    //origin and destination name, taken from the crb ID
    String originName, destinationName, originId, destinationId;
    private float requestDurationValue;
    HashMap<String, String> originNames;
    HashMap<String, String> destinationNames;

    //boolean from the resources to discover if is a tablet size or not
    boolean isTablet = false;
    
  //Text to Speech class
  	public TextToSpeech tts;
	private static Locale LANGUAGE = Locale.US;
	private int queueMode = TextToSpeech.QUEUE_ADD;
	private HashMap<String, String> params = new HashMap<String, String>();
	private static final float TTS_SPEECHRATE = 1.0f;
	
	//Speech Recognition service
	private int mBindFlag;
	private Messenger serviceMessenger;
	Intent service;
	
	//new activity Intent
	Intent finalActivityIntent;
	

    
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------LIFE CYCLE METHODS-----------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results); //load the xml UI accordingly to the screen size (layout or layout-large
        
        //prepare the Intent for the new 
        
        //Connect the service to the current activity
        service = new Intent(this, SpeechRecognitionService.class);
        this.startService(service);
        mBindFlag = Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH ? 0 : Context.BIND_ABOVE_CLIENT;
        
        
        
        //VARIABLE OF THE BUNDLE, AND IF IS A TABLET OR NOT
        bundle = this.getIntent().getExtras(); //get the solutions array from the other activity
        isTablet = getResources().getBoolean(R.bool.isTablet); //I set a variable in the resources, in order to split the decives in tablet and handset
    	
        
    	if (bundle != null) {
    		//get data from the previous activity via bundle
    		solutions = (ArrayList<Solution>) bundle.getSerializable(JSONHelper.TAG_BUNDLE_SOLUTIONS);
    		originDestinationOptions = solutions.get(0).getOriginDestinationOptions(); //for the solutions, I take all the segments
    		tempOriginDestinationOptions = originDestinationOptions; //store a copy for the reload
        	originName = bundle.getString(JSONHelper.TAG_ORIGIN_NAME);
        	originId = bundle.getString(JSONHelper.TAG_ORIGIN_ID);
        	originNames = (HashMap<String, String>) bundle.getSerializable(JSONHelper.TAG_ORIGIN_NAMES);
        	
        	destinationName = bundle.getString(JSONHelper.TAG_DESTINATION_NAME);
        	destinationId = bundle.getString(JSONHelper.TAG_DESTINATION_ID);
        	dateString = bundle.getString(JSONHelper.TAG_DEPARTURE);
        	destinationNames = (HashMap<String, String>) bundle.getSerializable(JSONHelper.TAG_DESTINATION_NAMES);
        	
        	requestDurationValue = bundle.getLong(JSONHelper.TAG_REQUEST_DURATION)/10; 
        	
        	//initialize the view elements
        	resumeInformationsFrom = (TextView) findViewById(R.id.TextView_resume_from_text);
        	resumeInformationsTo = (TextView) findViewById(R.id.TextView_resume_to_text);
        	resumeDate = (TextView) findViewById(R.id.TextView_resume_date_string);
        	cheapestImageButton = (ImageButton) findViewById(R.id.button_cheapest);
        	shortestImageButton = (ImageButton) findViewById(R.id.button_shortest);
        	closestImageButton = (ImageButton) findViewById(R.id.button_closest);  
        	arrivalTimeImageButton = (ImageButton) findViewById(R.id.button_closest_arrival);
        	reloadButton = (ImageButton) findViewById(R.id.button_reload);
        	requestDuration = (TextView) findViewById(R.id.textView_request_duration);
        }        
    }
    
    

    /*
     * When the activity starts, I call the methods to create or refresh the fragments view.
     * 
     */
    @Override
	protected void onStart() {
		super.onStart();
		//initialize the tts
		//initialize and start the TTS engine
		tts = new TextToSpeech(this, this);
		
		//the resume view is in common
		//set into the TextView the date
		resumeInformationsFrom.setText(WordHelper.capitalizeFirstLetter(originName));
		resumeInformationsTo.setText(WordHelper.capitalizeFirstLetter(destinationName));
		resumeDate.setText(dateString);
	
		
		//TABLET UI
    	//since the fragment is already loaded, I have to updated the view with the solution adapter
    	if (isTablet) {	
    		//start the service the first time
    		startService();
    		
    		
    		
    		//the duration of the request
    		requestDuration.setText("Amadeus Rails Cache service: " + requestDurationValue + "ms");
			
    		final SolutionListFragment solutionListFragment = (SolutionListFragment) 
														getSupportFragmentManager().findFragmentById(R.id.solutionlist_fragment);
			
			//if exist, I update the data inside without recreating the entire fragment
			if (solutionListFragment != null) {			
				solutionListFragment.startSolutionListFragment(originDestinationOptions); //displays all options for a trip
			}

    		//button filters
        	cheapestImageButton.setOnClickListener(new OnClickListener() {
        		
				
				@Override
				public void onClick(View v) {
					solutionListFragment.startSolutionListFragment(FilterHelper.filteredByPrice(originDestinationOptions));
					OriginDestinationOptions cheapestSolution = FilterHelper.filteredByPrice(originDestinationOptions).get(0);
					String TTSText = FilterHelper.getStringForTTSFiltering(cheapestSolution);
					
					TTSSentences("The cheapest solution " + TTSText );
					cheapestImageButton.setSelected(true);
					shortestImageButton.setSelected(false);
					closestImageButton.setSelected(false);
					arrivalTimeImageButton.setSelected(false);
				}
			});          	
        	shortestImageButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					solutionListFragment.startSolutionListFragment(FilterHelper.filteredByDuration(originDestinationOptions));
					OriginDestinationOptions shortestSolution = FilterHelper.filteredByDuration(originDestinationOptions).get(0);
					String TTSText = FilterHelper.getStringForTTSFiltering(shortestSolution);
					
					
					TTSSentences("The shortest solution " + TTSText);
					cheapestImageButton.setSelected(false);
					shortestImageButton.setSelected(true);
					closestImageButton.setSelected(false);
					arrivalTimeImageButton.setSelected(false);
					
					
				}
			});           	
        	closestImageButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					solutionListFragment.startSolutionListFragment(FilterHelper.filteredByDepartureTime(originDestinationOptions));
					OriginDestinationOptions closestSolution = FilterHelper.filteredByDepartureTime(originDestinationOptions).get(0);
					String TTSText = FilterHelper.getStringForTTSFiltering(closestSolution);
					
					
					TTSSentences("The closest solution " + TTSText);
					cheapestImageButton.setSelected(false);
					shortestImageButton.setSelected(false);
					closestImageButton.setSelected(true);	
					arrivalTimeImageButton.setSelected(false);
				}
			});
        	arrivalTimeImageButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					solutionListFragment.startSolutionListFragment(FilterHelper.filteredByArrivalTime(originDestinationOptions));
					OriginDestinationOptions arrivalTimeSolution = FilterHelper.filteredByArrivalTime(originDestinationOptions).get(0);
					String TTSText = FilterHelper.getStringForTTSFiltering(arrivalTimeSolution);
					
					
					TTSSentences("The closest arrival solution " + TTSText); 
					
					cheapestImageButton.setSelected(false);
					shortestImageButton.setSelected(false);
					closestImageButton.setSelected(false);	
					arrivalTimeImageButton.setSelected(true);	
				}
			});
        	
        	
        	//button reload data
        	reloadButton.setOnClickListener(new OnClickListener() {
    			
    			@Override
    			public void onClick(View v) {
    				finish();
    				startActivity(getIntent());
    			}
    		});
    	}
    	
    	
    	
    	
    	
    	//SMARTPHONE UI
    	else {
    		//creation of an array with the first ordered property
    		originDestinationBestOnes.add(FilterHelper.filteredByPrice(originDestinationOptions).get(0));
    		originDestinationBestOnes.add(FilterHelper.filteredByDuration(originDestinationOptions).get(0));
    		originDestinationBestOnes.add(FilterHelper.filteredByDepartureTime(originDestinationOptions).get(0));
    				
			//start the fragment list, with the array of the best solutions
			loadSolutionListFragment(originDestinationBestOnes);
			
    	}	
	}
 
    @Override
	protected void onDestroy() {
    	//when the activity is destroyed, the resources of tts and speech service are released
    	if (tts != null) {
    		tts.stop();
    		tts.shutdown();
    	}
    	if (speechServiceConnection != null) {
    		unbindService(speechServiceConnection);
    		this.stopService(service);
    	}
    	if (broadcastReceiver != null) {
    		unregisterReceiver(broadcastReceiver);
    	}
		super.onDestroy();	
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------- METHODS FOR THE TTS--------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    @Override
	public void onInit(int status) {
    	if (status != TextToSpeech.ERROR) {
			params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "fitlersUtterance");
			tts.setLanguage(LANGUAGE);
		}
	}
    
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------- SERVICE CONNECTION AND BROADCAST RECEIVER--------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private final ServiceConnection speechServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d("SERVICE CONNECTION", "onServiceConnected");
			serviceMessenger = new Messenger(service);
			Message msg = new Message();
			msg.what = SpeechRecognitionService.MSG_RECOGNIZER_START_LISTENING;
			
			try{
				serviceMessenger.send(msg);
			}catch (RemoteException e){
				
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d("SERVICE DISCONNECTION", "onServiceDisconnected");
			serviceMessenger = null;
			
		}
    	
    };
    
    
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
    	String textParsed = null;
		@Override
		public void onReceive(Context context, Intent intent) {
			textParsed = intent.getStringExtra(JSONHelper.TAG_TEXT);
			//here I receive the text from the speech recognition service. In this scenario onReceive is always "ON"
			if (textParsed != null) {
				Log.v("text parsed", textParsed);
				String current_command = Commands.find(textParsed); //methods of the enum able to return the family of a set of keywords
				if (current_command != null) {
					Log.v("commands", current_command);
					//basing on the "family" of the words, the buttons are triggered
					if (current_command.equalsIgnoreCase(Commands.CHEAPEST.toString())) {
						cheapestImageButton.performClick();
					}
					else if (current_command.equalsIgnoreCase(Commands.SHORTEST.toString())) {
						shortestImageButton.performClick();
					}
					else if (current_command.equalsIgnoreCase(Commands.CLOSEST.toString())) {
						closestImageButton.performClick();
					}
					else if (current_command.equalsIgnoreCase(Commands.CLOSEST_ARRIVAL.toString())) {
						arrivalTimeImageButton.performClick();
					}
					else if (current_command.equalsIgnoreCase(Commands.CONFIRM.toString())) {
						startFinalActivity(originDestinationOptions.get(0));
					}
				}
			}
		} 	
    };
    
    
    private void stopService() {
    	unbindService(speechServiceConnection);
    	stopService(service);
    	
    }
    
    private void startService() {
    	//bind the speech service with this activity
    	bindService(service, speechServiceConnection, mBindFlag);
		registerReceiver(broadcastReceiver, new IntentFilter(SpeechRecognitionService.BROADCAST_ACTION)); //the receiver intercepts the intent messages 
																										  //returned by the service
		startService(service);
    }
    
    private void TTSSentences(String sentence) {
    	if (tts != null){
    		if (tts.isSpeaking()){
    			tts.stop();
    		}
    		
    		tts.setSpeechRate(TTS_SPEECHRATE);
    		tts.speak(sentence, queueMode, params); //tts for the sentence coded in the enum				
    		tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
    			
    			@Override
    			public void onStart(String utteranceId) {
    				stopService();
    				
    			}
    			
    			@Override
    			public void onError(String utteranceId) {
    				// TODO Auto-generated method stub
    				
    			}
    			
    			@Override
    			public void onDone(String utteranceId) {
    				startService();
    				
    			}
    		});
    	}
    }
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------- METHODS FOR UPDATING THE FRAGMENT VIEW--------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /*
     * method for replacing the the framelayout with the fragment
     */
    public void loadSolutionListFragment(ArrayList<OriginDestinationOptions> originDestinationOptions) {
    	//start the fragment list, with the array of the best solutions
		SolutionListFragment solutionsList = new SolutionListFragment();
		Bundle extras = new Bundle();
		extras.putSerializable(JSONHelper.TAG_BUNDLE_OPTIONS, originDestinationOptions);
		solutionsList.setArguments(extras);
		// Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction()
                .add(R.id.listview_container, solutionsList).commit();
        
    }
    
    /*
     * on callback the details view is created or updated, depending if it is necessary to start a new activity or to update an existing one
     */
    public void onSolutionSelected(int position) {
    	selectedSolution = originDestinationOptions.get(position);
    	
    	//capture the single item with details fragment from the activity layout
    	if (isTablet) {
    		//update the view of the detail fragment, already exists in the xml view
    		DetailsFragment detailsFragment = (DetailsFragment) 
    											getSupportFragmentManager().findFragmentById(R.id.details_fragment);
    		if (detailsFragment != null) {
    			detailsFragment.mapDataToDetailItem(selectedSolution, originId, originName, originNames, destinationId, destinationName, destinationNames);
    		}	
    	}
    	else {  //SMARTPHONE
    		DetailsFragment detailsFragment = new DetailsFragment();
    		Bundle bundle = new Bundle();
    		bundle.putSerializable(JSONHelper.TAG_BUNDLE_OPTION, originDestinationBestOnes.get(position));
    		bundle.putString(JSONHelper.TAG_DESTINATION_ID, destinationId);
    		bundle.putString(JSONHelper.TAG_DESTINATION, destinationName);
    		bundle.putString(JSONHelper.TAG_ORIGIN_ID, originId);
    		bundle.putString(JSONHelper.TAG_ORIGIN, originName);
    		
            //start a new fragment, passing the selected solution
    		detailsFragment.setArguments(bundle);
    		getSupportFragmentManager().beginTransaction()
    			.add(R.id.listview_container, detailsFragment)
    			.addToBackStack(null)
    			.commit();		
    	}
    }


    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------- METHODS FOR STARTING FINAL ACTIVITY--------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public void startFinalActivity(OriginDestinationOptions selectedSolution) {
		Intent finalIntent = new Intent(this, FinalActivity.class);
		finalIntent.putExtra(JSONHelper.TAG_ORIGIN_NAME, originName);
		finalIntent.putExtra(JSONHelper.TAG_DESTINATION_NAME, destinationName);
		finalIntent.putExtra(JSONHelper.TAG_DEPARTURE, dateString);
		
		if (selectedSolution != null) {
			finalIntent.putExtra(JSONHelper.TAG_DEPARTURE_TIME, selectedSolution.JourneySegments.get(0).departureDateTime);
			finalIntent.putExtra(JSONHelper.TAG_ARRIVAL_TIME, selectedSolution.JourneySegments.get(0).arrivalDateTime);
			finalIntent.putExtra(JSONHelper.TAG_AMOUNT, selectedSolution.getPricings().get(0).getAmounterAfterTax().getAmount());
			finalIntent.putExtra(JSONHelper.TAG_CURRENCY, selectedSolution.getPricings().get(0).getAmounterAfterTax().getCurrency());
			
			
			if (finalIntent != null)
				this.startActivity(finalIntent);
		}
	}
}