package com.example.androidhttpgetservlet.activities;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.example.androidhttpgetservlet.R;
import com.example.androidhttpgetservlet.enums.Currencies;
import com.example.androidhttpgetservlet.helpers.JSONHelper;
import com.example.androidhttpgetservlet.helpers.TimeHelper;
import com.example.androidhttpgetservlet.helpers.WordHelper;

public class FinalActivity extends Activity {
		
	TextView textViewFinalSentence;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_final);
		
		Intent i = getIntent();
		Bundle extras = i.getExtras();
		
		
		if(extras != null) {
			String originName = WordHelper.capitalizeFirstLetter(extras.getString(JSONHelper.TAG_ORIGIN_NAME));
			String destinationName = WordHelper.capitalizeFirstLetter(extras.getString(JSONHelper.TAG_DESTINATION_NAME));
			String departureDate = extras.getString(JSONHelper.TAG_DEPARTURE);
			String departureTime = TimeHelper.getReadableDateTime(extras.getString(JSONHelper.TAG_DEPARTURE_TIME));
			String arrivalTime = TimeHelper.getReadableDateTime(extras.getString(JSONHelper.TAG_ARRIVAL_TIME));
			String amount = extras.getString(JSONHelper.TAG_AMOUNT);
			String currency = extras.getString(JSONHelper.TAG_CURRENCY);
			currency = Currencies.find(currency);
			
			
			
			
			
			
			int light_blue = getResources().getColor(R.color.light_blue); //get the color from the resources
			int violet_amadeus = getResources().getColor(R.color.violet_amadeus);
			
			String finalSentence = "Your train from  " + WordHelper.changeColor(originName, light_blue) +  " to " 
									+ WordHelper.changeColor(destinationName, light_blue) + " will leave the " + 
									WordHelper.changeColor(departureDate, light_blue) + " at " + 
									WordHelper.changeColor(departureTime, light_blue) + " and arrive at " +
									WordHelper.changeColor(arrivalTime, light_blue) +
									". The cost of your ticket is " + 
									WordHelper.changeColor(amount + " " + currency, violet_amadeus);
			
			textViewFinalSentence = (TextView) findViewById(R.id.textView_final_sentence);
//			Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Track.ttf");
//	 		if (tf != null)
//	 			textViewFinalSentence.setTypeface(tf);
			textViewFinalSentence.setText(Html.fromHtml(finalSentence));
		}
	}
	
	
	

}
