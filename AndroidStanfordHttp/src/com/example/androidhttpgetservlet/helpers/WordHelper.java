package com.example.androidhttpgetservlet.helpers;

import android.app.Activity;
import android.widget.Toast;

public class WordHelper {

	private WordHelper(){}
	
	public static String capitalizeFirstLetter(String sentence) {
		final StringBuilder result = new StringBuilder(sentence.length());
		sentence = sentence.toLowerCase();
		String[] words = sentence.split("\\s");
		for (int i = 0; i<words.length; i++) {
			if (i > 0) result.append(" ");
			result.append(Character.toUpperCase(words[i].charAt(0)))
				  .append(words[i].substring(1));
		}
		
		return result.toString();
	}

	public static void printToast(Activity activity, String message) {
		Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
	}

	public static String changeColor(String word, int colorHex) {
		return "<font color='" + colorHex + "'>" + word + "</font>";	
	}
}

