package com.example.androidhttpgetservlet.helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.example.androidhttpgetservlet.models.City;
import com.example.androidhttpgetservlet.models.Solution;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Static class for parsing the results from the JSON object
 * @return HashMap with all the information readable for java
 * @author dbattaglino 
 *
 */

public class JSONHelper {
	
	//keys for post request
	private static final String TAG_MESSAGE = "message";
	private static final String VALUE_TAG_ID = "01";
    private static final String TAG_UTF = "utf-8";
		
	//keys of the json and the hashmap
	public static final String TAG_ORIGIN = "origin";
	public static final String TAG_ORIGIN_ID = "origin_id";
	public static final String TAG_DESTINATION = "destination";
	public static final String TAG_DESTINATION_ID = "destination_id";
	public static final String TAG_TEXT = "text";
	public static final String TAG_LATITUDE = "latitude";
	public static final String TAG_LAT = "lat";
	public static final String TAG_LONGITUDE = "longitude";
	public static final String TAG_LNG = "lng";
	public static final String TAG_DEPARTURE = "departure";
	public static final String TAG_TICKETS = "tickets";
	public static final String TAG_TICKET_CLASS = "ticket_class";
    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_DEPARTURE_TIME = "departure_time";
    public static final String TAG_ARRIVAL_TIME = "arrival_time";
    public static final String TAG_AMOUNT = "amount";
    public static final String TAG_CURRENCY = "currency";
    
    //keys for the json response for the Amadeus solutions
    public static final String TAG_START_DATE = "start_date";
    public static final String TAG_CLASS = "class";
    public static final String TAG_SEARCHENGINERESPONSE = "SearchEngineResponse";
    public static final String TAG_AMA_RAILTRIPFINDERRS = "AMA_RailTripFinderRS";
    public static final String TAG_ORIGINDESTINATIONINFORMATION = "OriginDestinationInformations";
	public static final String TAG_ORIGINDESTINATIONOPTIONS = "OriginDestinationOptions";
	public static final String TAG_ERRORS = "Errors";
	public static final String TAG_ERROR_CODE = "Code";
	public static final String TAG_ERROR_MESSAGE = "Text";

	
	//url params to add to the get message
	public static final String RID = "1234";
	public static final String SITE_CODE = "thack";
	public static final String API_KEY = "thack-london-june-2012";
	public static final String LOCALE = "UK";
	
	//keys for data passing between two activities
    public static final String TAG_BUNDLE_SOLUTIONS = "solutions";
    public static final String TAG_BUNDLE_RESULTS = "results";
    public static final String TAG_BUNDLE_OPTIONS = "options";
    public static final String TAG_BUNDLE_OPTION = "option";
    public static final String TAG_REQUEST_DURATION = "duration";
    
    
    
    //keys for the intent of origin city and destination city
    public static final String TAG_ORIGIN_NAME = "origin_name";
    public static final String TAG_DESTINATION_NAME = "destination_name";
    public static final String TAG_SELECTED_SOLUTION = "selected_solution";
    public static final String TAG_SELECTED_POSITION = "selected_position";
	private static final String TAG_SUGGEST = "suggest";
	public static final String TAG_ORIGIN_NAMES = "origin_names";
	public static final String TAG_DESTINATION_NAMES = "destination_names";
	private static final String TAG_QUERY = "query";

	
	//set time connection of the requests
	private static int timeoutConnection = 5000;
	private static int timeoutSocketConnection = 3000;
	
	//language of the recognizer
	public static final String LOCALE_LANGUAGE = Locale.UK.toString();
	
	/*
	 * static void constructor
	 */
	private JSONHelper () {}
	
	
	
	//---------------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------JSON GENERAL METHODS------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	/*
	 * Retrieves the POST message response, and parses the JSON from Tomcat server
	 */
	public static HashMap<String, Object> getJSONfromurl(String url, String text, double latitude, double longitude) throws Exception {
		String JSONString = null;
		
        JSONString = executeHttpPost(url, text, latitude, longitude);
        //automatically parsing the results in objects called solution
        return parseJSON(JSONString);
	}
	
	
    /*
     * Handles the data from Amadeus solution
     */
    public static ArrayList<Solution> getJSONTrainsSolutions (String url, String origin_id, String destination_id, String start_date, String tickets_class) throws  Exception {
        String JSONString = null;
        ArrayList<Solution> solutions = new ArrayList<Solution>();

        //gets the json string from the
        JSONString = executeHttpGet(url, origin_id, destination_id, start_date, tickets_class);

        //automatically mapped into object structure
        Gson gson = new Gson();
        JsonParser jsonParser = new JsonParser();
        JsonObject mainJSONObject = (JsonObject) jsonParser.parse(JSONString);
        if (!mainJSONObject.isJsonNull()) {
            JsonObject searchEngineResponse = mainJSONObject.getAsJsonObject(TAG_SEARCHENGINERESPONSE);
           if (!searchEngineResponse.isJsonNull()) {
               JsonObject AMA_RailTripFinderRS = searchEngineResponse.getAsJsonObject(TAG_AMA_RAILTRIPFINDERRS);
               if (AMA_RailTripFinderRS != null) {
            	    if (!AMA_RailTripFinderRS.isJsonNull()) {
                        JsonArray originDestinationInformations = AMA_RailTripFinderRS.getAsJsonArray(TAG_ORIGINDESTINATIONINFORMATION);
                        if (!originDestinationInformations.isJsonNull()) {	 
     						for (JsonElement jsonElement: originDestinationInformations) {
     						    Solution solution = gson.fromJson(jsonElement, Solution.class);
     						    solutions.add(solution);
     						}
                        }
                    }
               }
               else {
            	   //it means there are errors
            	   JsonObject errors = searchEngineResponse.getAsJsonObject(TAG_ERRORS);
            	   if (!errors.isJsonNull()) {
            		   Solution solution = new Solution();
            		   solution.setError_code(errors.get(TAG_ERROR_CODE).getAsString());
            		   solution.setError_message(errors.get(TAG_ERROR_MESSAGE).getAsString());
            		   solutions.add(solution);
            	   }
               }
           }
       }
       return solutions;
    }

    public static String getNameCityJSON (String url, String id) throws Exception {
    	String JSONString = null;
    	
    	JSONString = executeHttpGet(url, id, true);
    	JSONArray jsonArray = new JSONArray(JSONString);
    	return jsonArray.getJSONObject(0).getString(TAG_NAME);
    }
    
    public static HashMap<String, String> getNameIdCityJSON (String url, String cityName) throws Exception{
    	String JSONString = null;
    	HashMap<String, String> JSONCitynames = new HashMap<String, String>();
    	
    	
    	JSONString = executeHttpGet(url, cityName, false);
    	JSONArray jsonArray = new JSONArray(JSONString);
    	if (jsonArray != null) {
    		for (int i=0; i < jsonArray.length(); i++) {
    			JSONObject currentCity = jsonArray.getJSONObject(i);
    			JSONCitynames.put(currentCity.getString(TAG_ID), WordHelper.capitalizeFirstLetter(currentCity.getString(TAG_NAME)));
    		}
    	}
    	return JSONCitynames;
    }
	
    public static City getCityFromCoordinates(String url, String latitude, String longitude) throws Exception {
    	String JSONString = null;
 
    	JSONString = executeHttpGet(url, latitude, longitude);
    	if (JSONString != null)
    		return parseClosestStation(JSONString);
    	else
    		return null;
    }

	//-----------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------JSON PARSER FOR TOMCAT AND FOR THE CLOSEST STATION------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	
	private static HashMap<String, Object> parseJSON (String JSONString) throws JSONException {
		HashMap<String, Object> results = new HashMap<String, Object>();
		
		try {
			JSONObject jsonObject = new JSONObject(JSONString);
			if (jsonObject != null) {
				JSONArray originJSONArray = jsonObject.optJSONArray(TAG_ORIGIN); //return the value if exists, null otherwise
				JSONArray destinationJSONArray = jsonObject.optJSONArray(TAG_DESTINATION); // return the value if exists, null otherwise
				
				if (originJSONArray != null) {
					HashMap<String, String> originHashMap = new HashMap<String, String>();
					originHashMap.put(TAG_TEXT, originJSONArray.getJSONObject(0).getString(TAG_TEXT));
					originHashMap.put(TAG_LATITUDE, originJSONArray.getJSONObject(1).getString(TAG_LATITUDE));
					originHashMap.put(TAG_LONGITUDE, originJSONArray.getJSONObject(2).getString(TAG_LONGITUDE));
					originHashMap.put(TAG_ID, originJSONArray.getJSONObject(3).getString(TAG_ID));
					results.put(TAG_ORIGIN, originHashMap);
				}
				
				if (destinationJSONArray != null) {
					HashMap<String, String> destinationHashMap = new HashMap<String, String>();
					destinationHashMap.put(TAG_TEXT, destinationJSONArray.getJSONObject(0).getString(TAG_TEXT));
					destinationHashMap.put(TAG_LATITUDE, destinationJSONArray.getJSONObject(1).getString(TAG_LATITUDE));
					destinationHashMap.put(TAG_LONGITUDE, destinationJSONArray.getJSONObject(2).getString(TAG_LONGITUDE));
					destinationHashMap.put(TAG_ID, destinationJSONArray.getJSONObject(3).getString(TAG_ID));
					results.put(TAG_DESTINATION, destinationHashMap);
				}
				
				if (jsonObject.has(TAG_DEPARTURE))
					results.put(TAG_DEPARTURE, jsonObject.getString(TAG_DEPARTURE));
				if (jsonObject.has(TAG_TICKETS)) 
					results.put(TAG_TICKETS, jsonObject.getString(TAG_TICKETS));
				if (jsonObject.has(TAG_TICKET_CLASS))
					results.put(TAG_TICKET_CLASS, jsonObject.getString(TAG_TICKET_CLASS));
				
			}	
		}
		catch (JSONException e) {	
			Log.e(JSONHelper.class.toString(), "" + e.getMessage());
		}
		
	//return the HashMap of the results extracted from the JSON	
	return results;
		
	}
		
	private static City parseClosestStation (String JSONString) throws JSONException {
		
		City city = null;
		try {
			JSONObject jsonObject = new JSONObject(JSONString);
			if (jsonObject != null) {
				Log.v("TAG_ID", "" + jsonObject.getString(TAG_ID));
				city = new City(jsonObject.getString(TAG_ID), jsonObject.getString(TAG_NAME));
			}
		}
		catch (JSONException e) {
			Log.e(JSONHelper.class.toString(), "" + e.getMessage());
		}
		
		return city;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------CONNECTION HANDLERS-------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
		
	
	private static String executeHttpGet(String url, String text, double latitude, double longitude) throws Exception {
		String JSONString = null;
		//set timeout for the connection	
		DefaultHttpClient httpClient = new DefaultHttpClient();	
		
		List<NameValuePair> nameValuePairs  = new LinkedList<NameValuePair>(); //NameValuePair --> structure used for POST message in Apache API
    	nameValuePairs.add(new BasicNameValuePair("rid", RID));
    	nameValuePairs.add(new BasicNameValuePair("site_code", SITE_CODE));
    	nameValuePairs.add(new BasicNameValuePair("api_key", API_KEY));
    	nameValuePairs.add(new BasicNameValuePair("locale", LOCALE));
    	nameValuePairs.add(new BasicNameValuePair("input_text", text));
    	nameValuePairs.add(new BasicNameValuePair("time_zone", "+02:00"));
    	nameValuePairs.add(new BasicNameValuePair("latitude", String.valueOf(latitude)));
    	nameValuePairs.add(new BasicNameValuePair("longitude", String.valueOf(longitude)));
    	String paramsString = URLEncodedUtils.format(nameValuePairs, TAG_UTF );
        url += paramsString; //building the get request in this way: base_url?params1=value&params2=value


    	HttpGet httpGet = new HttpGet(url);
    	HttpResponse httpResponse = httpClient.execute(httpGet); //ClientHttp executes the GET request
    	HttpEntity httpEntity = httpResponse.getEntity();
    	if (httpEntity != null) 
    		JSONString = EntityUtils.toString(httpEntity);
        		          
        return JSONString;
	
	}


    /*
     * execute the request to amadeus with all the solution for a given origin/destination at a certain date
     */
    private static String executeHttpGet (String url, String origin_id, String destination_id, String start_date, String tickets_class)  throws Exception{
        String JSONString = null;
        //set timeout for the connection
        DefaultHttpClient httpClient = new DefaultHttpClient();

        List<NameValuePair> nameValuePairs = new LinkedList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair(TAG_ORIGIN, origin_id));
        nameValuePairs.add(new BasicNameValuePair(TAG_DESTINATION, destination_id));
        nameValuePairs.add(new BasicNameValuePair(TAG_START_DATE, start_date));
        nameValuePairs.add(new BasicNameValuePair(TAG_CLASS, "00" + tickets_class));
        String paramsString = URLEncodedUtils.format(nameValuePairs, TAG_UTF);
        url += paramsString;
        Log.v("URL", url);

        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        if (httpEntity != null) {
            JSONString = EntityUtils.toString(httpEntity);
        }

        return JSONString;
    }
	
	
	private static String executeHttpPost(String url, String text, double latitude, double longitude) throws ParseException, IOException {
		
		String JSONString = null;
		//set timeout for the connection
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection); //time for the connection
		HttpConnectionParams.setSoTimeout(httpParams, timeoutSocketConnection); //time waiting for the response		
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
        HttpPost httpPost = new HttpPost(url);
        httpPost.setParams(httpParams); //start the timeouts
        
       
    	List<NameValuePair> nameValuePairs  = new ArrayList<NameValuePair>(); //NameValuePair --> structure used for POST message in Apache API
    	nameValuePairs.add(new BasicNameValuePair(TAG_ID, VALUE_TAG_ID));
    	nameValuePairs.add(new BasicNameValuePair(TAG_MESSAGE, text));
    	nameValuePairs.add(new BasicNameValuePair(TAG_LATITUDE, String.valueOf(latitude)));
    	nameValuePairs.add(new BasicNameValuePair(TAG_LONGITUDE, String.valueOf(longitude)));
    	httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));  // encode the POST request with the url and message parameters
    	
    	HttpResponse httpResponse = httpClient.execute(httpPost); //ClientHttp executes the POST request
    	HttpEntity httpEntity = httpResponse.getEntity();
    	if (httpEntity != null) 
    		JSONString = EntityUtils.toString(httpEntity);
    	
    	return JSONString;
	}
	
	
	private static String executeHttpGet(String url, String field, boolean isId)  throws android.net.ParseException, IOException{
		String JSONString = null;
        //set timeout for the connection
        DefaultHttpClient httpClient = new DefaultHttpClient();

        List<NameValuePair> nameValuePairs = new LinkedList<NameValuePair>();
        if (isId)
        	nameValuePairs.add(new BasicNameValuePair(TAG_ID, field)); 
        else
        	nameValuePairs.add(new BasicNameValuePair(TAG_QUERY, field));
        String paramsString = URLEncodedUtils.format(nameValuePairs, TAG_UTF);
        url += paramsString;
        Log.v("URL", "" + url);

        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        if (httpEntity != null) {
            JSONString = EntityUtils.toString(httpEntity);
        }

        return JSONString;
		
	}
	
	
	private static String executeHttpGet(String url, String latitude, String longitude) throws ClientProtocolException, IOException {
		String JSONString = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		
		List<NameValuePair> nameValuePairs = new LinkedList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair(TAG_LAT, latitude));
		nameValuePairs.add(new BasicNameValuePair(TAG_LNG, longitude));
		String paramsString = URLEncodedUtils.format(nameValuePairs, TAG_UTF);
		url += paramsString;
		Log.v("URL", "" + url);

        HttpGet httpGet = new HttpGet(url);
        HttpResponse httpResponse = httpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();
        if (httpEntity != null) {
            JSONString = EntityUtils.toString(httpEntity);
        }

        return JSONString;
	}
}	


