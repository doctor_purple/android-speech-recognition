package com.example.androidhttpgetservlet.helpers;

import java.util.ArrayList;
import java.util.Collections;
import com.example.androidhttpgetservlet.models.Solution.OriginDestinationOptions;


public class FilterHelper {
	private FilterHelper() {}
	
	public static ArrayList<OriginDestinationOptions> filteredByPrice (ArrayList<OriginDestinationOptions> originDestinationOptions) {
		
		Collections.sort(originDestinationOptions); //overwrite the results in OriginDestinationOptionss
		return originDestinationOptions;
	}
	
	public static ArrayList<OriginDestinationOptions> filteredByDuration (ArrayList<OriginDestinationOptions> originDestinationOptions) {
		Collections.sort(originDestinationOptions, new OriginDestinationOptions.DurationComparator());
		return originDestinationOptions;
	}
	
	public static ArrayList<OriginDestinationOptions> filteredByDepartureTime (ArrayList<OriginDestinationOptions> originDestinationOptions) {
		Collections.sort(originDestinationOptions, new OriginDestinationOptions.TimeComparator());
		return originDestinationOptions;
	}
	
	public static ArrayList<OriginDestinationOptions> filteredByArrivalTime (ArrayList<OriginDestinationOptions> originDestinationOptions) {
		Collections.sort(originDestinationOptions, new OriginDestinationOptions.ArrivalTimeComparator());
		return originDestinationOptions;
	}
	
	public static String getStringForTTSFiltering(OriginDestinationOptions currentSolution) {
		String departureTime = TimeHelper.getSpeechableDateTime(currentSolution.getJourneySegments().get(0).departureDateTime);
		String arrivalTime = TimeHelper.getSpeechableDateTime(currentSolution.getJourneySegments().get(0).arrivalDateTime);
		String price = currentSolution.getPricings().get(0).getAmounterAfterTax().getAmount();
		String currencyCode = currentSolution.getPricings().get(0).getAmounterAfterTax().getCurrency();
		String currency = "euros";
		if (currencyCode.equals("CAD")) {
			currency = "dollars";
		}
		
		return "costs " + price + " " + currency + " , it starts at " + departureTime + "  and it arrives at " + arrivalTime;
	}
}
