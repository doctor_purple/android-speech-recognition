 package com.example.androidhttpgetservlet.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

import android.util.Log;

/*
 * Helpers able to compute the time difference, parsing in different format and strings
 */
public class TimeHelper {
	private static final String pattern = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String resultPattern = "%02d:%02d";
	
	
	private TimeHelper () {}
	
	public static String computeDuration(String departureTime, String arrivalTime) {
		Date departureTimeDate = null; 
		Date arrivalTimeDate = null;
		
		try {
			departureTimeDate = new SimpleDateFormat(pattern).parse(departureTime);
			arrivalTimeDate = new SimpleDateFormat(pattern).parse(arrivalTime);
		} catch (ParseException e) {
			
			Log.e("Date parsing" , e.getMessage());
		}
		
		
		if (departureTimeDate != null && arrivalTimeDate != null) {
			long diffInMillies = computeDifference(departureTimeDate, arrivalTimeDate);
			long diffInMinutes = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
			int hours = (int) (diffInMinutes / 60); //hours 
			int minutes = (int) (diffInMinutes % 60); //minutes
			return "" + hours + "H" + minutes;
		}
		else
			return "error in parsing the date!";
		
	}
	
	public static int computeDurationInMinutes(String departureTime, String arrivalTime) {
		Date departureTimeDate = null; 
		Date arrivalTimeDate = null;
		
		try {
			departureTimeDate = new SimpleDateFormat(pattern).parse(departureTime);
			arrivalTimeDate = new SimpleDateFormat(pattern).parse(arrivalTime);
		} catch (ParseException e) {
			
			Log.e("Date parsing" , e.getMessage());
		}
		
		
		if (departureTimeDate != null && arrivalTimeDate != null) {
			long diffInMillies = arrivalTimeDate.getTime() - departureTimeDate.getTime();
			long diffInMinutes = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
			return  (int) diffInMinutes;
		}
		else
			return 0;
		
	}
	
	public static long computeDifference(Date date1, Date date2) {
		return date2.getTime() - date1.getTime();	
	}
	
	public static Date getDateFromString(String date) throws ParseException {
		return new SimpleDateFormat(pattern).parse(date);
	}
	
	public static String  getReadableDateTime (String time) {
		Date currentTime = null;
		try {
			currentTime = new SimpleDateFormat(pattern).parse(time);
		} 
		catch (ParseException e) {
			Log.e("Date parsing" , e.getMessage());
		}
		
		if (currentTime != null) {
			Calendar c = GregorianCalendar.getInstance();
			c.setTime(currentTime);
			String readableDateTime = String.format(resultPattern,c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE));
			return readableDateTime;
		}
		else 
			return "Not readable data";		
	}

	public static String getSpeechableDateTime(String time) {
		Date currentTime = null;
		try {
			currentTime = new SimpleDateFormat(pattern).parse(time);
		} 
		catch (ParseException e) {
			Log.e("Date parsing" , e.getMessage());
		}
		
		if (currentTime != null) {
			Calendar c = GregorianCalendar.getInstance();
			c.setTime(currentTime);
			int hours = c.get(Calendar.HOUR);
			int minutes = c.get(Calendar.MINUTE);
			String am_pm;
			if(c.get(Calendar.AM_PM) == 0)  
				am_pm = "AM";  
			else  
				am_pm = "PM";  
			
			return Integer.toString(hours) + " " + Integer.toString(minutes) + " " + am_pm; 
		}
		else 
			return "Not readable data";	
		
	}

}
