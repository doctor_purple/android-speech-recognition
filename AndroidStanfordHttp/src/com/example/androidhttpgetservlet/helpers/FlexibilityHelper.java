package com.example.androidhttpgetservlet.helpers;

public class FlexibilityHelper {
	
	public FlexibilityHelper () {}
	
	public static String codeToString(String code) {
		int i = Integer.valueOf(code);
		
		switch(i)  {
		case 1:
			return "Unknown flexibility options";
		case 2:
			return "Exchangeable, Refundable";
		case 3:
			return "Exchangeable, Not Refundable";
		case 4:
			return "Not Exchangeable, Not Refundable";
		case 5:
			return "Exchangeable With Fee, Refundable Before Departure";
		case 6:
			return "Not Exchangeable, Refundable";
		case 7:
			return "Exchangeable, Refundable With Conditions";
		default:
			return "Unknown flexibility code";
		}	
	}
}
