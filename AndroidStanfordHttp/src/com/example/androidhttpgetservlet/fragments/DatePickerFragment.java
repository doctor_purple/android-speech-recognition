package com.example.androidhttpgetservlet.fragments;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.DatePicker;




/**
 * DatePicker fragments, allows the user to change manually the settings of the calendar
 * @author dbattaglino
 *
 */
public class DatePickerFragment extends DialogFragment implements OnDateSetListener { 
	private int year, month, day;
	private Button triggeringButton; //button that triggered the call to this dialog
	private Calendar c; //Calendar object
	private String dateFormatted;
	
	/*
	 * Takes the button that has triggered the dialogs
	 */
	public DatePickerFragment (Button triggeringButton, Calendar c) {
		this.triggeringButton = triggeringButton;
		this.c = c;
	}


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		//Use the current time as the default value for the picker
		if (c == null) 
			c = new GregorianCalendar();
		
		//extraction of the year, mont, day of month
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		
		return new DatePickerDialog(getActivity(), this, year, month, day);	
	}

	
	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		//when the date is set, 
		changeDateButton(triggeringButton, year, month, day);	
	}

	
	//-----------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------PRIVATE METHODS-----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	
	//set the current time to the label on the button
	private void changeDateButton(Button button, int year, int month, int day) {
		button.setText(new StringBuilder()
				.append(day)
				.append("/")
				.append(month + 1)
				.append("/")
				.append(year));	
		
		
	}
}
