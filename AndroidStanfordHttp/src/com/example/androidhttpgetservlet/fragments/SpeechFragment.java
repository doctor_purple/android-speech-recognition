package com.example.androidhttpgetservlet.fragments;


import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.androidhttpgetservlet.MainActivity;
import com.example.androidhttpgetservlet.R;
import com.example.androidhttpgetservlet.enums.TTSsentences;
import com.example.androidhttpgetservlet.helpers.JSONHelper;
import com.example.androidhttpgetservlet.helpers.WordHelper;
import com.example.androidhttpgetservlet.interfaces.OnClickSpeechListener;


public class SpeechFragment extends Fragment implements RecognitionListener {
	//callback for starting the fragment
	OnClickSpeechListener activityCallback;
	
	//static variables (URLS, TAGS, general settings)
	public static final String URL_TOMCAT = "http://172.16.247.212:8080/StanfordNLPServlet/MainServlet"; //URL in the local network where the Apache server is set
	public static final String URL_EVATURE = "http://freeapi.evature.com/v1.0?"; //URL of the Evature system
	public static final String TAG = "ANDROID_STANFORD_HTTP"; //tag label for the android log
	public static final String  TAG_BUNDLE_RESULTS = "results"; //tag for the hashmap key
	public static final int RESULT_SPEECH = 1; //static int for result speech
	public static final float MAX_VOLUME = 12f; //the number in dB to be divided to the volume perceived 
	
	
	//View elements
	private Button overlayButton;
	private View speechView;
	//private TextView speechResults;
	private EditText speechResults;
	private FieldsFragment filedsFragment;
	
	//HashMap for JSON data
	HashMap <String, Object> results = new HashMap<String, Object>(); 
	
	//ArrayList for words returned from Google Speech system
	ArrayList<String> googleSpeech_results;
	
	//speech Recognition class
	private SpeechRecognizer speechRecognizer;
	Intent intent;
	boolean isListening = false;
	
	//location for the origin
	double longitude, latitude;
	
	//Connection manager
	ConnectivityManager connectivityManager;
	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------LIFE CYCLES METHODS----------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE); //connection manager
		
		
		//iniziate the view elements
		speechView =  inflater.inflate(R.layout.view_speech, container, false); //sets the view to the fragment
		speechResults = (EditText) speechView.findViewById(R.id.speechResults_textView);
		overlayButton = (Button) speechView.findViewById(R.id.button_overlay);
		overlayButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//check the connection before call the speech recognizer activity
				if (isConnected()) {
					if (!isListening) {
						isListening = true;
						startSpeechRecognizer(); //start the speech recognizer
					}
					else if (isListening) {
						isListening = false;
						stopSpeechRecognizer(); //stop the speech recognizer
					}
				}

				else {
					WordHelper.printToast(getActivity(),"Not connected to internet!");
				}

                //asynctask that transforms a natural text in a json object (origin, destination, date, class, number of tickets)
//                GetNLPJsonTask task = new GetNLPJsonTask();
//                if (speechResults.getText() != null) 
//                	task.execute(new String[] {URL_TOMCAT, "two tickets for paris"}); //start the asynctask for Tomcat service
                //task.execute(new String[] {URL_EVATURE, googleSpeech_results.get(0)}); //start the asynctask for Eva service
			}
		});
		
		return speechView;
	}
	
	/*
	 * Checks before the starting, if the interface methods are implemented
	 */
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            activityCallback = (OnClickSpeechListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnClickSpeechListener");
        }
    }



	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------HTTP METHODS IN ASYNC TASK---------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/*
	 * starts another thread parallel to UI, able to call the servlet process at the URL specified
	 */
	private class GetNLPJsonTask extends AsyncTask<String, Void, HashMap<String, Object>> {
	    private Exception NLPException = null;


		protected HashMap<String,Object> doInBackground(String... params) {
	        String url = params[0];
	        String text = params[1];
			
	        try {
	        	
	        	//this two methods are for retrieve data from two different sources
	        	results = JSONHelper.getJSONfromurl(url, text, ((MainActivity)getActivity()).latitude, ((MainActivity)getActivity()).longitude); //get Json from NLP Stanford
			} catch (Exception e) {
				NLPException = e;
			} 
			
	        //return statement
	        return results;
				   
	    }
	    
	   
		protected void onPostExecute(HashMap <String, Object> results) {
		   
			stopSpeechRecognizer();  
			//set the background to the original blue color
			if (NLPException != null)
				WordHelper.printToast(getActivity(), NLPException.getMessage());
			else {
				if (results != null) {
					//start the intent  of another activity, sending the object with all data inside
					Bundle bundle = new Bundle();
					bundle.putSerializable(TAG_BUNDLE_RESULTS, results);
					activityCallback.startResultsFragment(bundle); //thanks to the interface, the method in the activity is called from this fragment 	 	
				} 
			}
		}	
	}
		
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------PRIVATE METHODS--------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
	/*
	 * Connection available
	 */
	private boolean isConnected() {
		NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
		if (ni == null) 
			return false;
		else return ni.isConnected();
	}
	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------SPEECH RECOGNIZER METHODS-----------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*
     * Initializes the intent for google speech recognizer
     */
	public void initializeSpeechIntent () {
		speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
		speechRecognizer.setRecognitionListener(this);
		
		intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		//this set the speech model to be recognized
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, JSONHelper.LOCALE_LANGUAGE);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, JSONHelper.LOCALE_LANGUAGE);
		intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
		intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);
		intent.putExtra("calling_package", getActivity().getPackageName());
	}
	
	
	/*
	 * Instruct the app to listen for user speech input
	 */
	public void startSpeechRecognizer(){
		initializeSpeechIntent();
		speechResults.setText(""); //reset the text 
		speechResults.setHint(""); //reset the hint 
		speechRecognizer.startListening(intent);
		
	}
	
	
	/*
	 * When the speech finishes, I close all the connection
	 */
	public void stopSpeechRecognizer() {
		if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.GINGERBREAD_MR1) 
			overlayButton.setAlpha(1.0f);
		else
			overlayButton.setBackgroundResource(R.drawable.speech_button_icon_overlay);
		
		speechResults.setHint(TTSsentences.SPEECH_HINT.toString());
		
		if (speechRecognizer != null) {
			speechRecognizer.stopListening();
			speechRecognizer.cancel();
			speechRecognizer.destroy();
			speechRecognizer = null;
		}
	
	}
	
	
	@Override
	public void onBeginningOfSpeech() {
		Log.v("speech", "beginning");
		filedsFragment = (FieldsFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.results_fragment);
		if (filedsFragment != null)
			filedsFragment.resetViewElements(); //resets all view elements, if we are in two-pane layout
	}


	@Override
	public void onBufferReceived(byte[] arg0) {
		// used for detecting if there is someone speaking
		Log.v("speech", "voice detected");
	}


	@Override
	public void onEndOfSpeech() {
		Log.v("speech", "end of speech");
		if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.GINGERBREAD_MR1)
			overlayButton.setAlpha(1.0f);	
	}


	@Override
	public void onError(int arg0) {
		Log.v("speech", "error");
		stopSpeechRecognizer();
	}


	@Override
	public void onEvent(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onPartialResults(Bundle partialData) {
		googleSpeech_results = partialData.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
		speechResults.setText(googleSpeech_results.get(0));
		
	}


	@Override
	public void onReadyForSpeech(Bundle arg0) {
		Log.v("speech", "ready for speech");
		speechResults.setVisibility(View.VISIBLE);
	
	}


	@Override
	public void onResults(Bundle data) {
		Log.v("speech", "onresult");
		stopSpeechRecognizer();
		
		if (data != null) {
			googleSpeech_results = data.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION); //list of all possible results from the most probable to the lower one
			String speechParsedText = googleSpeech_results.get(0);
			speechResults.setText(speechParsedText);
			
			//asynctask	
			GetNLPJsonTask task = new GetNLPJsonTask(); 
			task.execute(new String[] {URL_TOMCAT, WordHelper.capitalizeFirstLetter(speechParsedText)}); //start the asynctask for Tomcat service with all the word capitalize to reduce the ambiguity
			//task.execute(new String[] {URL_EVATURE, googleSpeech_results.get(0)}); //start the asynctask for Eva service
		}
		
		stopSpeechRecognizer();
	}


	@Override

	public void onRmsChanged(float rmsdB) {
		if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.GINGERBREAD_MR1) {
			overlayButton.setAlpha(rmsdB/MAX_VOLUME);
		}
		else {
			//Log.v("values for background", ""+  (rmsdB/MAX_VOLUME)* 255);
			overlayButton.setBackgroundColor(Color.TRANSPARENT);
			overlayButton.getBackground().setAlpha(Math.round(rmsdB/MAX_VOLUME)* 255);
		}
	}
}
