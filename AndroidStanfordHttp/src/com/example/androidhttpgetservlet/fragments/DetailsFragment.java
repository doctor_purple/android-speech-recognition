package com.example.androidhttpgetservlet.fragments;


import java.util.HashMap;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import com.example.androidhttpgetservlet.R;
import com.example.androidhttpgetservlet.adapters.DetailAdapter;
import com.example.androidhttpgetservlet.helpers.JSONHelper;
import com.example.androidhttpgetservlet.models.Solution.OriginDestinationOptions;

/**
 * 
 * @author dbattaglino
 * Details fragment is the page of the details, with the method mapDatatoDetailitem used by external classes
 */
public class DetailsFragment extends Fragment {
	
	View v;
	ListView detailsListView;
	DetailAdapter detailAdapter;
	
	//animations
	Animation animationFadeIn; 
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------LIFE CYCLE METHODS-----------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.view_details, container, false);
		return v;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			OriginDestinationOptions selectedOption = (OriginDestinationOptions) bundle.getSerializable(JSONHelper.TAG_BUNDLE_OPTION);
			String originId = bundle.getString(JSONHelper.TAG_ORIGIN_ID);
			String originName = bundle.getString(JSONHelper.TAG_ORIGIN);
			HashMap<String, String> originNames = bundle.getParcelable(JSONHelper.TAG_ORIGIN_NAMES);
			String destinationId = bundle.getString(JSONHelper.TAG_DESTINATION_ID);
			String destinationName = bundle.getString(JSONHelper.TAG_DESTINATION);
			HashMap<String, String> destinationNames = bundle.getParcelable(JSONHelper.TAG_ORIGIN_NAMES);
			
			
			if (selectedOption != null) {
				mapDataToDetailItem(selectedOption, originId, originName, originNames, destinationId, destinationName, destinationNames);
			}
		}
	}


	
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------UPDATE VIEW-----------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/*
	 * Updates the view, either when the view is created or called from tablet
	 */
	public void mapDataToDetailItem (OriginDestinationOptions selectedOption, String originId, String originName, HashMap<String, String> originNames,  String destinationId,  String destinationName, HashMap<String, String> destinationNames) {
		//get the listView of this section, and call the adapter
		if (v != null) {
			detailsListView = (ListView) v.findViewById(R.id.listView_details);
			animationFadeIn = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fade_in);
			detailsListView.startAnimation(animationFadeIn);
			detailsListView.setAdapter(new DetailAdapter(getActivity(), selectedOption, originId, originName, originNames, destinationId, destinationName, destinationNames));					
		}
		
	}
	
	
	

}
