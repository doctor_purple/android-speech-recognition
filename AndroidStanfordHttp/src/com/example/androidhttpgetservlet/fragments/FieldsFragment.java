package com.example.androidhttpgetservlet.fragments;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


import com.example.androidhttpgetservlet.MainActivity;
import com.example.androidhttpgetservlet.R;
import com.example.androidhttpgetservlet.activities.DisplayResultsActivity;
import com.example.androidhttpgetservlet.adapters.SuggestionAdapter;
import com.example.androidhttpgetservlet.enums.Commands;
import com.example.androidhttpgetservlet.enums.TTSsentences;
import com.example.androidhttpgetservlet.helpers.JSONHelper;
import com.example.androidhttpgetservlet.helpers.WordHelper;
import com.example.androidhttpgetservlet.models.City;
import com.example.androidhttpgetservlet.models.Solution;
import com.example.androidhttpgetservlet.threads.CityNameTask;
import com.example.androidhttpgetservlet.threads.ClosestCityNameTask;

public class FieldsFragment extends Fragment implements RecognitionListener, OnInitListener{
	
	//View
	private Button startDatePickerButton; 
	private ImageButton confirmButton;
	private Button overlayButton;
	private EditText ticketsEditText, classEditText;
	private AutoCompleteTextView originEditText, destinationEditText;
	private long init,now,time;

	//Data from the the speech fragment
	private HashMap<String, Object> results;
	private Calendar c;
	private Bundle bundle = new Bundle();
	
	//Static variables (used for testing the solution of amadeus, I'm sure that with these i will get some results to display
    private static final String URL_SOLUTIONS = "http://172.24.46.12:21157/1ASIRAILSE/RAILDS?";
    private static final String URL_CITY = "http://172.16.133.118:8095/CRBConnector/crb/suggest?";
    private static final String URL_CLOSEST_STATION = "http://172.16.133.118:8095/CRBConnector/crb/getClosestStation?";
   
    private static final float TTS_SPEECHRATE = 1f;
    private static final float MAX_VOLUME = 12f;
    private static final int AUTOCOMPLETE_LETTERS_THRESHOLD = 3;

    //New intent to call a new activity
    private Intent displayResults;

    //Data structure for the solutions provided from Amadeus
    private ArrayList<Solution> solutions;
    private String origin_id, origin_name, destination_id, destination_name, start_date, tickets_class;
    private String latitude, longitude;
    private City closestStation;
    
    //Speech recognition class and other fragments
    private SpeechRecognizer speechRecognizer;
    private Intent intent;
    private SpeechFragment speechFragment;
    
    //ArrayList for words returned from Google Speech system
  	private ArrayList<String> googleSpeech_results;
  	
  	//Text to Speech class
  	public TextToSpeech tts;
	private static Locale LANGUAGE = Locale.US;
	private int queueMode = TextToSpeech.QUEUE_ADD;
	private HashMap<String, String> params = new HashMap<String, String>();
	
	//Asynctask
	private getJSONAmadeusSolutionTask solutionTask;
	private ClosestCityNameTask closestCityNameTask;
	

  	
  	//-----------------------------------------------------------------------------------------------------------------------------------------
  	//---------------------------------------------------LIFE CYCLE METHODS--------------------------------------------------------------------
  	//-----------------------------------------------------------------------------------------------------------------------------------------
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, 
	        Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//inflate the layout of results for this fragment
		View resultsView = inflater.inflate(R.layout.view_results, container, false);
		resultsView.clearFocus(); //clear all focus from the page
		
		//return the view to the next lifecycle states
		return resultsView;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		//initialize and start the TTS engine
		tts = new TextToSpeech(getActivity(), this);
		bundle = getArguments();
		mapDataToEditText(bundle);
		speechFragment = (SpeechFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.speech_fragment);
	}
	
	
	


	@Override
	public void onDestroy() {
		super.onDestroy();
		if (tts != null) {
			tts.stop();
			tts.shutdown();
			tts = null;
		}
	}	


	//-----------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------UPDATE VIEW---------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	/*
	 * Maps all the data in the bundle in the edit text, starts the dialog for the date
	 */
	@SuppressWarnings("unchecked")
	public void mapDataToEditText (Bundle bundle) {	
		//reset variables
		resetVariables();
		
		
		//get the data from the bundle
		try {
			results = (HashMap<String, Object>) bundle.get(JSONHelper.TAG_BUNDLE_RESULTS);
		}
		catch (Exception e) {
			Log.e("Error at bundle", e.toString());
		}


        //I put the results displayed into the intent
        displayResults = new Intent(getActivity(), DisplayResultsActivity.class);
        displayResults.putExtra(JSONHelper.TAG_BUNDLE_RESULTS, results);
		
		//initializes the edit text, and buttons
		originEditText = (AutoCompleteTextView) getActivity().findViewById(R.id.editText_origin);
		destinationEditText = (AutoCompleteTextView) getActivity().findViewById(R.id.EditText_destination);
		ticketsEditText = (EditText) getActivity().findViewById(R.id.editText_tickets);
		classEditText = (EditText) getActivity().findViewById (R.id.EditText_class);
		startDatePickerButton = (Button) getActivity().findViewById(R.id.button_datePickerDialog);
		confirmButton = (ImageButton) getActivity().findViewById(R.id.button_confirm);
		overlayButton = (Button) getActivity().findViewById(R.id.button_overlay);
		
		
		//display the first time the origin city if there are coordinates
		latitude = Double.toString(((MainActivity)getActivity()).latitude);
		longitude = Double.toString(((MainActivity)getActivity()).longitude);
		if (latitude != null && longitude != null) {	
			closestCityNameTask = new ClosestCityNameTask();
			try {
				closestStation = closestCityNameTask.execute(new String[] {URL_CLOSEST_STATION, latitude, longitude }).get();
				if (closestStation != null) {
					origin_id = closestStation.getcrbCode();
					origin_name = closestStation.getCityName();
					originEditText.setText(WordHelper.capitalizeFirstLetter(origin_name)); 	
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		
		//Starts the date picker dialog above the normal activity flow
		startDatePickerButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				DatePickerFragment datePickerFragment = new DatePickerFragment(startDatePickerButton, c);
				datePickerFragment.show(getFragmentManager(), "datePicker");
			}
		});

		
		if (results != null) {
			//map the values into the fields and handle the exceptions in the missing fields
			//ORIGIN
			if (results.get(JSONHelper.TAG_ORIGIN) != null) {
				if (results.get(JSONHelper.TAG_ORIGIN) instanceof HashMap<?, ?>) {
					HashMap<String, String> origin = (HashMap<String, String>) results.get(JSONHelper.TAG_ORIGIN);
					origin_id = origin.get(JSONHelper.TAG_ID);
					origin_name = WordHelper.capitalizeFirstLetter(origin.get(JSONHelper.TAG_TEXT));
					originEditText.setInputType(android.text.InputType.TYPE_CLASS_TEXT);
					originEditText.setText(origin_name); //map the name of the origin, if exists	
				}
			}
			//DESTINATION
			if (results.get(JSONHelper.TAG_DESTINATION) != null) {
				if (results.get(JSONHelper.TAG_DESTINATION) instanceof HashMap<?, ?>) {
					HashMap<String, String> destination = (HashMap<String, String>) results.get(JSONHelper.TAG_DESTINATION);
					destination_id = destination.get(JSONHelper.TAG_ID);
					destination_name = WordHelper.capitalizeFirstLetter(destination.get(JSONHelper.TAG_TEXT));
					destinationEditText.setInputType(android.text.InputType.TYPE_CLASS_TEXT);
					destinationEditText.setText(destination_name); //map the name of the destination, if exists
				}
			}
			else {
				if (tts != null) {
					Log.v("TTS", "IS NOT NULL");
					if (origin_id != null && destination_id != null && start_date != null && tickets_class != null) {
						//CONFIRMATION FOR THE VOICE IF 
						tts.setSpeechRate(TTS_SPEECHRATE);
						tts.speak(TTSsentences.CONFIRM.toString(),queueMode, params); //tts for the sentence coded in the enum	
						tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
							
			
							@Override
							public void onStart(String arg0) {
							}
							
							@Override
							public void onError(String utteranceId) {
							}
							
							@Override
							public void onDone(String utteranceId) {
									getActivity().runOnUiThread(new Runnable() {
										@Override
										public void run() {
											startConfirmationRecognizer();
										}
									});								
							}
						});	
					}
				}
				tts.speak(TTSsentences.DESTINATION.toString(), queueMode, params);
				//the new instance of the speech starts now
				if (speechFragment != null) {
					tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {

						@Override
						public void onDone(String arg0) {
							getActivity().runOnUiThread(new Runnable() {

								@Override
								public void run() {
									speechFragment.startSpeechRecognizer();
								}
							});
							
						}

						@Override
						public void onError(String arg0) {
								
						}

						@Override
						public void onStart(String arg0) {
							speechFragment.stopSpeechRecognizer();	
						}	
					});		
				}
			}
			
			//TICKETS
			if (results.get(JSONHelper.TAG_TICKETS) != null) {
				Object tickets = results.get(JSONHelper.TAG_TICKETS);
				if (tickets instanceof String) {
					ticketsEditText.setText((String) tickets);
				}
			}
			
			//CLASS 
			if (results.get(JSONHelper.TAG_TICKET_CLASS) != null) {
				Object class_tickets = results.get(JSONHelper.TAG_TICKET_CLASS);
				if (class_tickets instanceof String) {
					classEditText.setText((String) class_tickets);
					tickets_class = (String) class_tickets;
				}
			}
			
			
			//DATE
			if (results.get(JSONHelper.TAG_DEPARTURE) != null) {
				Object departure = results.get(JSONHelper.TAG_DEPARTURE);
				if (departure instanceof String) {
					start_date = (String) departure;
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					try {
						Date date = dateFormat.parse((String) departure);
						c = new GregorianCalendar();
						c.setTime(date);
						changeDateButton(startDatePickerButton, c);
						
					} catch (ParseException e) {
						Log.e("Date parsing", e.getMessage());
					}
				}
			}
			if (tts != null) {
				if (origin_id != null && destination_id != null && start_date != null && tickets_class != null) {
					//CONFIRMATION FOR THE VOICE IF 
					tts.setSpeechRate(TTS_SPEECHRATE);
					tts.speak(TTSsentences.CONFIRM.toString(),queueMode, params); //tts for the sentence coded in the enum	
					tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
						
		
						@Override
						public void onStart(String arg0) {
						}
						
						@Override
						public void onError(String utteranceId) {
						}
						
						@Override
						public void onDone(String utteranceId) {
								getActivity().runOnUiThread(new Runnable() {
									@Override
									public void run() {
										startConfirmationRecognizer();
									}
								});								
						}
					});	
				}
			}		
		}
				
		//autosuggestion block for origin and destination
		final SuggestionAdapter originSuggestionAdapter = new SuggestionAdapter(getActivity(), originEditText.getText().toString(), URL_CITY);
		originEditText.setThreshold(AUTOCOMPLETE_LETTERS_THRESHOLD);
		originEditText.setAdapter(originSuggestionAdapter);
		originEditText.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long arg3) {
				Log.v("ITEM ORIGIN", "selected crb: " + originSuggestionAdapter.getItemAtPosition(position));
				origin_id = originSuggestionAdapter.getItemAtPosition(position);
				origin_name = originSuggestionAdapter.getItem(position);
				
			}
		});

		final SuggestionAdapter destinationSuggestionAdapter = new SuggestionAdapter(getActivity(), destinationEditText.getText().toString(), URL_CITY);
		destinationEditText.setThreshold(AUTOCOMPLETE_LETTERS_THRESHOLD);
		destinationEditText.setAdapter(destinationSuggestionAdapter);
		destinationEditText.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position,
						long arg3) {
					Log.v("ITEM DESTINATION", "selected crb: " + destinationSuggestionAdapter.getItemAtPosition(position));
					destination_id = destinationSuggestionAdapter.getItemAtPosition(position);
					destination_name = destinationSuggestionAdapter.getItem(position);
					destinationSuggestionAdapter.notifyDataSetChanged();
					
					//if the destination is not null the startDatePicker is updated, with the number of tickets and the class
					if (destination_id != null && destination_name != null) {
						c = new GregorianCalendar();
						changeDateButton(startDatePickerButton, c);
						ticketsEditText.setText("1");
						classEditText.setText("2");
						tickets_class = "2";
						start_date = getFormattedString();
					}
				}
			});
		
		
		
		//clicking on the confirmation button
				confirmButton.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						startResultActivity();
					}
				});
		
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------TOOLS METHODS--------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	
	/*
	 * Sets the current time in the Calendar c to the button in the view
	 */
	private void changeDateButton(Button button, Calendar c) {
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);
		
		button.setText(new StringBuilder()
				.append(day)
				.append("/")
				.append(month + 1)
				.append("/")
				.append(year));
	}
	
	/*
	 * Transforms the date on the button in a date format string
	 */
	private String getFormattedString() {
		String onTheButton = startDatePickerButton.getText().toString();
		String [] fields = onTheButton.split("/");
		int day = Integer.parseInt(fields[0]);
		int month = Integer.parseInt(fields[1]) - 1;
		int year = Integer.parseInt(fields[2]);
		
		Log.v("formatted date", day + " " + month + " " + year);
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		c.set(year, month, day);
		return df.format(c.getTime());		
	}
	
	/*
	 * Every time a speech recognizer is started, the fields are emptied
	 */
	public void resetViewElements() {
		//initializes the edit text, and buttons
				originEditText.setText("");
				destinationEditText.setText("");
				ticketsEditText.setText("");
				classEditText.setText("");
	}
	
	/*
	 * Resets the variable every time the activity is refreshed
	 */
	public void resetVariables() {
		origin_id = null;
		destination_id = null;
		origin_name = null;
		origin_name = null;
		start_date = null;
	}
	
	/*
	 * starts the display activity results (either after button clicked, or of the user says "confirm"
	 */
	public void startResultActivity() {
		//initialize the asynctask
		 solutionTask = new getJSONAmadeusSolutionTask(); //start the task for the Amadeus_solution
		//start a new activity for displaying the results, check if there is at least the destination
        if (results != null) {
            //START A RESULTS ACTIVITY
        	if (destination_id != null && destination_name != null && origin_id != null && origin_name != null) {
            	confirmButton.setImageResource(R.drawable.ok_button_pressed);
            	solutionTask.execute(new String[] {URL_SOLUTIONS}); //start the new activity, after getting back the solution proposals    
            }
        }
        //in the case of not-results from speech, check if the destination id and name are present
        else {
        	if (destination_id != null && destination_name != null && origin_id != null && origin_name != null) {
        		confirmButton.setImageResource(R.drawable.ok_button_pressed);
        		solutionTask.execute(new String[] {URL_SOLUTIONS});
        	}
        }
	}
	
	/*
	 * When the tts starts, it sets the initial settings
	 */
	@Override
	public void onInit(int status) {
		if (status != TextToSpeech.ERROR) {
			params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "myUtterance");
			tts.setLanguage(LANGUAGE);
		}
	}
	
    //-----------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------HTTP METHODS IN ASYNC TASK-------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------
    private class getJSONAmadeusSolutionTask extends AsyncTask<String, Void, ArrayList<Solution>> {
    	private Exception trainsSolutionException = null; 
    	
        protected ArrayList<Solution> doInBackground(String... params) {
            String url = params[0];
            //retrieve data from Amadeus and send it to new activity onPostExecute
            try {
            	init = System.currentTimeMillis(); //current time when the request is done
            	solutions = JSONHelper.getJSONTrainsSolutions(url, origin_id, destination_id, start_date, tickets_class); //json request to amadeus for the trains solution  
            } 
            catch (Exception e) {
            	trainsSolutionException = e;	
            }

            return solutions;
        }

        protected void onPostExecute(ArrayList<Solution> solutions) {
        	//CATCH THE EXCEPTION
        	if (trainsSolutionException != null) {
        		WordHelper.printToast(getActivity(), trainsSolutionException.getMessage());
        		//change background
            	confirmButton.setImageResource(R.drawable.ok_button);
        	}
        	
        	else {
        		if (solutions != null) {
                	//duration of the solutions request
                	now = System.currentTimeMillis();
            		time = now - init;
            		displayResults.putExtra(JSONHelper.TAG_REQUEST_DURATION, time);
                	
            		//get the city names with two http requests, in the post execute because the two calls for the name of the cities follow the first requests for solutions 
                	CityNameTask originNameTask = new CityNameTask();
                	CityNameTask destinationNameTask = new CityNameTask();
                	try {
    					HashMap<String, String> originNames = originNameTask.execute(new String[] {URL_CITY, origin_name}).get();  //execution of http request to get the originName
    					if (originNames != null) {
    						displayResults.putExtra(JSONHelper.TAG_ORIGIN_NAME, origin_name); //put the extra informations in the intent, in order to be passed to the next activity
    						displayResults.putExtra(JSONHelper.TAG_ORIGIN_ID, origin_id); //put the origin id, in order to be checked in the next activity
    						displayResults.putExtra(JSONHelper.TAG_ORIGIN_NAMES, originNames); //put the extra informations also the destination id

    					}
    					HashMap<String, String> destinationNames = destinationNameTask.execute(new String[] {URL_CITY, destination_name}).get();  //execution of http request to get the destinationName
    					if (destinationNames != null) {
    						displayResults.putExtra(JSONHelper.TAG_DESTINATION_NAME, destination_name); //put the extra informations in the intent in order to be passes to the next activity
    						displayResults.putExtra(JSONHelper.TAG_DESTINATION_ID, destination_id); //put the extra informations also the destination id
    						displayResults.putExtra(JSONHelper.TAG_DESTINATION_NAMES, destinationNames); //put the extra informations also the destination id

    					}
    					if (start_date != null) {
    						displayResults.putExtra(JSONHelper.TAG_DEPARTURE, start_date);
    					}
    									
    				} catch (InterruptedException e) {	
    					e.printStackTrace();
    				} catch (ExecutionException e) {
    					e.printStackTrace();
    				}
                	
                	//detect the error message in the solutions
                	if (solutions.get(0).getError_code() == null) {
                		displayResults.putExtra(JSONHelper.TAG_BUNDLE_SOLUTIONS, solutions); //put the solutions from Amadeus cache into the intent for being used in the new activity          
                    	displayResults.putExtra(JSONHelper.TAG_BUNDLE_RESULTS, results);
                    	startActivity(displayResults); //start the activity
                    	
                	}
                	else {
                		//should display the error from the solutions
                		WordHelper.printToast(getActivity(), solutions.get(0).getError_message());
                		
                	}
            		 	
                	//change background
                	confirmButton.setImageResource(R.drawable.ok_button);
                }
                else {
                	confirmButton.setImageResource(R.drawable.ok_button_pressed);
                }
        	}         
 		}
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------SPEECH RECOGNITION COMMANDS------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------
    /*
     * Initializes the intent for google speech recognizer
     */
    private void initializeSpeechIntent () {
		speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
		speechRecognizer.setRecognitionListener(this);
		
		intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		//this set the speech model to be recognized
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, JSONHelper.LOCALE_LANGUAGE);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, JSONHelper.LOCALE_LANGUAGE);
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Confirm your choice?");
		intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
		intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);
		intent.putExtra("calling_package", getActivity().getPackageName());
	}  
    
    public void startConfirmationRecognizer() {
	   initializeSpeechIntent();
	   if (speechRecognizer != null) {
			speechRecognizer.startListening(intent); //when it starts, the ovverride methods are tirggered
		}
	}		
	
    public void stopSpeechRecognizer() {
		if (speechRecognizer != null) {
			speechRecognizer.stopListening();
			speechRecognizer.cancel();
		}
		overlayButton.setAlpha(1f);
	}  
	
    @Override
	public void onBeginningOfSpeech() {
		Log.v("speech", "beginning");
	}

	@Override
	public void onBufferReceived(byte[] buffer) {
		Log.v("speech", "voice detected");
		
	}

	@Override
	public void onEndOfSpeech() {
		Log.v("speech", "end of speech");
		
	}

	@Override
	public void onError(int error) {
		stopSpeechRecognizer();
		
	}

	@Override
	public void onEvent(int eventType, Bundle params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPartialResults(Bundle partialResults) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReadyForSpeech(Bundle params) {
		Log.v("speech", "ready for speech");
		
	}
	
	@Override
	public void onResults(Bundle data) {
		Log.v("speech", "onresult");
		stopSpeechRecognizer();
		
		if (data != null) {
			googleSpeech_results = data.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION); //list of all possible results from the most probable to the lower one
			Log.v("confirmation results","" + googleSpeech_results);
			if (googleSpeech_results != null) {
				String current_command = Commands.find(googleSpeech_results.get(0).toString());
				if (current_command != null)
					startResultActivity();
			}
		}	
	}
	
	@Override
	public void onRmsChanged(float rmsdB) {
		//Log.v("Volume Changed", "" + rmsdB/MAX_VOLUME);
		overlayButton.setAlpha(rmsdB/MAX_VOLUME);
	}



}
