package com.example.androidhttpgetservlet.fragments;

import java.util.ArrayList;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.example.androidhttpgetservlet.R;
import com.example.androidhttpgetservlet.adapters.SolutionAdapter;
import com.example.androidhttpgetservlet.gestures.SwipeDetector;
import com.example.androidhttpgetservlet.helpers.JSONHelper;
import com.example.androidhttpgetservlet.interfaces.OnClickSolutionItemListener;
import com.example.androidhttpgetservlet.interfaces.OnDeleteItemListener;
import com.example.androidhttpgetservlet.models.Solution.OriginDestinationOptions;

public class SolutionListFragment extends ListFragment {
	OnClickSolutionItemListener onItemClickCallback;
	
	//data structure of the solutions
	ArrayList<OriginDestinationOptions> originDestinationOptions;
	ArrayList<OriginDestinationOptions> tempOriginDestinationOptions;
	
	//static variables
	private static final int DIVIDER_HEIGTH = 15; //height between the list item
	private static final int ITEM_PADDING = 10; //internal padding of the list view
	boolean isShortFilteredList = false; //case of the smartphone, with the list already filtered
	
	//swiping gesture
	private SwipeDetector swipeDetector;
	
	//view elements
	private ListView lv;
	private SolutionAdapter solutionAdapter;
	
	
	
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
  	//---------------------------------------------------LIFE CYCLE METHODS--------------------------------------------------------------------
  	//-----------------------------------------------------------------------------------------------------------------------------------------
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			
		
		Bundle bundle = getArguments();
		if (bundle != null) {
			//origin destination options is the array specular to the json objects of the solutions
			originDestinationOptions = (ArrayList<OriginDestinationOptions>) bundle.getSerializable(JSONHelper.TAG_BUNDLE_OPTIONS); 
			//fill the list when the activity is created
			if (originDestinationOptions != null)
				isShortFilteredList = true;
			
			startSolutionListFragment(originDestinationOptions); //updates the view with the adapter of the solutions	
		}
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		
		
		lv = getListView();
		//when a deletion is detected, the item at the i-th position is removed, and the view refreshed
		swipeDetector = new SwipeDetector(lv, 
				new OnDeleteItemListener() {
					
					@Override
					public void onDelete(ListView listView, int[] sortedPositions) {
						for (int position : sortedPositions) {
							//originDestinationOptions.remove(position);
							if (tempOriginDestinationOptions.size() > 0) {
								tempOriginDestinationOptions.remove(position);
								if (getFragmentManager().findFragmentById(R.id.details_fragment) != null && position == 0) 
									onItemClickCallback.onSolutionSelected(0); //start the animation of the details fragment, only if there are two panes layout and the position to delete is the first one
									solutionAdapter.setSelectedPosition(0);
							}	
						}
						solutionAdapter.notifyDataSetChanged(); //update the adapter automatically
					}
					
					
					@Override
					public boolean canDelete(int position) {
						if (position == 0 && tempOriginDestinationOptions.size() == 1) //prevent the complete deletion of the list elements.There is always at least one element
							return false;
						//in the other case it returns 'deletable'
						return true;
					}
				});
		lv.setOnTouchListener(swipeDetector);
		lv.setOnScrollListener(swipeDetector.makeScrollListener());	//scroll listener adapted to the swipe gesture
		lv.setDivider(null);
		lv.setDividerHeight(DIVIDER_HEIGTH); //distance between items
		lv.setPadding(ITEM_PADDING, 0 ,ITEM_PADDING, 0); //internal padding respect to the items and the list view
		lv.setCacheColorHint(Color.TRANSPARENT); 
		lv.setSelector(R.drawable.solution_item_selector);
	}
	
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented 
        // the callback interface. If not, it throws an exception.
        try {
            onItemClickCallback = (OnClickSolutionItemListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnClickSpeechListener");
        }
    }
	
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		onItemClickCallback.onSolutionSelected(position);
		//highligh only if there are two panes layout
		if (getFragmentManager().findFragmentById(R.id.details_fragment) != null) {
			solutionAdapter.setSelectedPosition(position);	
			solutionAdapter.notifyDataSetChanged();
		}	
	}
	
	

	//--------------------------------------------------------------------------------------------------------------------------------------------
	//------------------------------------------ METHODS FOR CALLING THE SOLUTION ADAPTER AND UPDATE THE VIEWS------------------------------------
	//--------------------------------------------------------------------------------------------------------------------------------------------
	
	/*
	 * This method can be called from external class. It's used to update the view in the tablet layout
	 */
	public void startSolutionListFragment(ArrayList<OriginDestinationOptions> originDestinationOptions) {
		this.originDestinationOptions = originDestinationOptions;
		this.tempOriginDestinationOptions = originDestinationOptions; //copy of the array
		solutionAdapter = new SolutionAdapter(getActivity(), tempOriginDestinationOptions, isShortFilteredList);
		setListAdapter(solutionAdapter); //call the solution adapter for the solution item in the list	
		if (getFragmentManager().findFragmentById(R.id.details_fragment) != null) {
			onItemClickCallback.onSolutionSelected(0); //at the beginning I load always the first elements 
		 }
	}
}
