package com.example.androidhttpgetservlet.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.androidhttpgetservlet.R;
import com.example.androidhttpgetservlet.activities.DisplayResultsActivity;
import com.example.androidhttpgetservlet.activities.FinalActivity;
import com.example.androidhttpgetservlet.helpers.TimeHelper;
import com.example.androidhttpgetservlet.models.Solution.OriginDestinationOptions;

/**
 * 
 * @author dbattaglino
 * Adapter for each solution element (creates the view given the options data)
 */
public class SolutionAdapter extends BaseAdapter {
	Context context;
	ArrayList<OriginDestinationOptions> originDestinationOptions;
	OriginDestinationOptions currentOriginDestinationOption;
	
	//static attributes
	private static LayoutInflater inflater = null;
	private static String SECOND = "002";
	private static String FIRST = "001";
	private static String CANADIAN_CURRENCY = "CAD";
	
	//SpannableString, used to change the font
	SpannableString spannableString; 
	
	//is a short filtered list used only on the smartphone
	private boolean isShortFilteredList = false;
	
	//selected item
	private int selectedPosition = 0;
	
	

	
	
	
	public SolutionAdapter (Context context, ArrayList<OriginDestinationOptions> originDestinationOptions, boolean isShortFilteredList) {
		this.context = context;
		this.originDestinationOptions = originDestinationOptions;
		this.isShortFilteredList = isShortFilteredList;
		
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); //allows to call and use the view from this class
	}
	
	/*
	 * in this case, the adapter knows the selected position and highlight only that one
	 */
	public void setSelectedPosition (int selectedPosition) {
		this.selectedPosition = selectedPosition;
	}
	
	@Override
	public int getCount() {
		return originDestinationOptions.size();
	}
	
	@Override
	public Object getItem(int position) {	
		return originDestinationOptions.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null)
			v = inflater.inflate(R.layout.solution_item, null);
		
		
		//view elements
		TextView timeTable = (TextView) v.findViewById(R.id.textView_timetable);
		TextView duration = (TextView) v.findViewById(R.id.textView_duration);
		TextView price = (TextView) v.findViewById(R.id.textView_price);
		TextView priceCents = (TextView) v.findViewById(R.id.textView_price_cents);
		TextView changesNumberView = (TextView) v.findViewById(R.id.TextView_changes_number);
		LinearLayout timeLinearLayout = (LinearLayout) v.findViewById(R.id.LinearLayout_solutionItem_time);
		LinearLayout priceLinearLayout = (LinearLayout) v.findViewById(R.id.LinearLayout_solutionItem_price);
		LinearLayout changesLinearLayout = (LinearLayout) v.findViewById(R.id.LinearLayout_solutionItem_changes);
		LinearLayout classLinearLayout = (LinearLayout) v.findViewById(R.id.LinearLayout_class);
		TextView classNumberTextView = (TextView) v.findViewById(R.id.TextView_class_number);
		TextView classTextTextView = (TextView) v.findViewById(R.id.TextView_class_text);
		ImageView euroIcon = (ImageView) v.findViewById(R.id.ImageView_euro_icon);
		ImageView timeIcon = (ImageView) v.findViewById(R.id.imageView_timeIcon);
		ImageView selectedSolutionCheck = (ImageView) v.findViewById(R.id.imageView_selected_solution);
		
		
		//set the data from solutions array --> these are the information for the total trip
		if (originDestinationOptions != null) {
			currentOriginDestinationOption = originDestinationOptions.get(position);
			String departureTime = currentOriginDestinationOption.getJourneySegments().get(0).departureDateTime;
			String arrivalTime =  currentOriginDestinationOption.getJourneySegments().get(0).arrivalDateTime;
			String amount = currentOriginDestinationOption.getPricings().get(0).getAmounterAfterTax().getAmount();
			String currency = currentOriginDestinationOption.getPricings().get(0).getAmounterAfterTax().getCurrency();
			String classTicket = currentOriginDestinationOption.getPricings().get(0).getAccomodation().getServiceClass();   
			
			
			
			//LAYOUT FOR THE SHORT LIST ON THE SMARTPHONE
			if (isShortFilteredList) {
				//chepeast
				if (position == 0) 					
					timeIcon.setImageResource(R.drawable.cheapest_button);							
				//shortest
				if (position == 1)
					timeIcon.setImageResource(R.drawable.shortest_button);
				//closest
				if (position == 2)
					timeIcon.setImageResource(R.drawable.closest_button);		
			}
			
			if (!isShortFilteredList) {	
				//LAYOUT FOR THE HIGHLIGH ITEM
				if (selectedPosition != position) {
					timeLinearLayout.setBackgroundResource(R.drawable.rectangular_container_upper_corners);
					priceLinearLayout.setBackgroundResource(R.drawable.rectangular_container_left_corner);
					changesLinearLayout.setBackgroundResource(R.drawable.rectangular_container_no_rounded_corner);	
					classLinearLayout.setBackgroundResource(R.drawable.rectangular_container_right_corner);
					selectedSolutionCheck.setClickable(false);
					selectedSolutionCheck.setAlpha(0f);
					
				}
				else {
					timeLinearLayout.setBackgroundResource(R.drawable.rectangular_container_upper_corners_green);
					priceLinearLayout.setBackgroundResource(R.drawable.rectangular_container_left_corner_green);
					changesLinearLayout.setBackgroundResource(R.drawable.rectangular_container_no_rounded_corner_green);	
					classLinearLayout.setBackgroundResource(R.drawable.rectangular_container_right_corner_green);
					selectedSolutionCheck.setClickable(true);
					selectedSolutionCheck.setAlpha(1f);
					
					//SELECTED SOLUTION CONFIRM
					selectedSolutionCheck.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							//Log.v("onclick solution", "position " + position);
								((DisplayResultsActivity)context).startFinalActivity(originDestinationOptions.get(position));
						}
					});
				}
			}
			
			
			
			//TIMETABLE
			timeTable.setText(TimeHelper.getReadableDateTime(departureTime) + " - " + TimeHelper.getReadableDateTime(arrivalTime));
			
			//DURATION
			duration.setText("(" + TimeHelper.computeDuration(departureTime, arrivalTime) + ")");
			
			//PRICE
			//I use the spannable class for modifying sub strings inside the view elements
			price.setText(amount);
			if (currency.equals(CANADIAN_CURRENCY)) {
				euroIcon.setImageResource(R.drawable.dollar_icon);
			}
		
			//CHANGES
			int numberChanges = currentOriginDestinationOption.getJourneySegments().size() - 1;
			if (numberChanges == 0) {
				changesNumberView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
				changesNumberView.setText("DIRECT");
			}
			else {
				changesNumberView.setText(numberChanges);
			}
			
			//CLASS ICON
			if (classTicket.equals(SECOND)) {
				classNumberTextView.setText("2");
				classTextTextView.setText("ND");
			}
			if(classTicket.equals(FIRST)) {
				classNumberTextView.setText("1");
				classTextTextView.setText("ST");
			}
		}
		return v;
	}	
}
