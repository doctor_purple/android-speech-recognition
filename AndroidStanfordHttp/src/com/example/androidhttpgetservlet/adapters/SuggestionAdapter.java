package com.example.androidhttpgetservlet.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.androidhttpgetservlet.models.City;
import com.example.androidhttpgetservlet.threads.CityNameTask;

public class SuggestionAdapter extends ArrayAdapter<String> implements Filterable {

	protected static final String SUGGESTION_TAG = "SuggestionAdapter";
	private List<City> suggestions;
	private String url;

	public SuggestionAdapter(Context context, String nameFilter, String url) {
		super(context, android.R.layout.simple_dropdown_item_1line);
		suggestions = new ArrayList<City>();
		this.url = url;
		
	}

	@Override
	public int getCount() {
		return suggestions.size();
	}
	
	@Override
	public String getItem(int position) {
		return suggestions.get(position).getCityName();	
	}
	

	public String getItemAtPosition(int position) {
		return suggestions.get(position).getcrbCode();
	}
	
	
	/*
	 * This metods implements the filtering over the list of citynames and crb codes
	 */
	public Filter getFilter() {
		Filter myFilter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				if (constraint != null) {
					if (url != null) {
						CityNameTask cityNameTask = new CityNameTask();
						try {
							HashMap<String, String> cityResults = cityNameTask.execute(new String[] {url, constraint.toString()}).get();
							suggestions.clear();
							
							Iterator it = cityResults.entrySet().iterator();
							while (it.hasNext()) {
								Entry pairs = (Entry) it.next();
								suggestions.add(new City(pairs.getKey().toString(), pairs.getValue().toString()));		
							}
							
							//now assigns the values and count the filterResults
							filterResults.values = suggestions;
							filterResults.count = suggestions.size();
							notifyDataSetChanged();
							
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (ExecutionException e) {
							e.printStackTrace();
						} 
					}
				}
				return filterResults;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				if (results != null && results.count > 0) {
					notifyDataSetChanged(); //refresh the view
				}
				else
					notifyDataSetInvalidated();
			}
			
		};
		return myFilter;
	}

}
