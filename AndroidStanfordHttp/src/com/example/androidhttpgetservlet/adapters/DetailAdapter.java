package com.example.androidhttpgetservlet.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.androidhttpgetservlet.R;
import com.example.androidhttpgetservlet.enums.Equipment;
import com.example.androidhttpgetservlet.enums.TrainCompanies;
import com.example.androidhttpgetservlet.helpers.FlexibilityHelper;
import com.example.androidhttpgetservlet.helpers.TimeHelper;
import com.example.androidhttpgetservlet.models.Solution.OriginDestinationOptions;
import com.example.androidhttpgetservlet.models.Solution.OriginDestinationOptions.JourneySegments;
import com.example.androidhttpgetservlet.threads.CityNameTask;


/**
 * 
 * @author dbattaglino
 * Class able to fill the informations about the segment
 */

public class DetailAdapter extends BaseAdapter {
	
	Context context;
	OriginDestinationOptions selectedOption;
	ArrayList<JourneySegments> segments;
	String destinationName, originName, originId, destinationId; //are initialized to null both
	private static final String URL_CITY = "http://172.16.133.118:8095/CRBConnector/crb/get?";
	private HashMap<String, String> originNames;
	private HashMap<String, String> destinationNames;
	
	//static attributes
	private static LayoutInflater inflater = null;
	private int selectedPosition = 0; 
	
	public DetailAdapter (Context context, OriginDestinationOptions selectedOption, String originId, String originName, HashMap<String, String> originNames,  String destinationId, String destinationName, HashMap<String, String> destinationNames) {
		this.context = context;
		this.selectedOption = selectedOption;
		this.originName = originName;
		this.originId = originId;
		this.originNames = originNames;
		this.destinationId = destinationId;
		this.destinationName = destinationName;
		this.destinationNames = destinationNames;
		segments = this.selectedOption.getJourneySegments(); //segments array with all the changes
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return segments.size();
	}

	@Override
	public Object getItem(int position) {
		
		return segments.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			v = inflater.inflate(R.layout.details_item, null);
		}


		//view elements
		TextView segmentDepartureTime = (TextView) v.findViewById(R.id.TextView_segment_departureTime);
		TextView segmentArrivalTime = (TextView) v.findViewById(R.id.TextView_segment_arrivalTime);
		TextView segmentDepartureStation = (TextView) v.findViewById(R.id.TextView_segment_departureStation);
		TextView segmentArrivalStation = (TextView) v.findViewById(R.id.TextView_segment_arrivalStation);
		TextView segmentEquipment = (TextView) v.findViewById(R.id.TextView_segment_equipment);
		ImageView segmentCompanyLogo = (ImageView) v.findViewById(R.id.ImageView_company_logo);
		TextView flexibility = (TextView) v.findViewById(R.id.textView_segment_flexibility);
	
		//set data for the segments
		if (segments != null) {
			JourneySegments currentSegment = segments.get(position); //takes only the selected position item 
			String departureTime = currentSegment.getDepartureDateTime();
			String arrivalTime = currentSegment.getArrivalDateTime();
			
			
			//handle the case when the departure station and the arrival station are equal to the origin and destination code
			String departureStationId = currentSegment.getDepartureStation();
			String departureStation = null;
			if (departureStationId.equals(originId)) 
				departureStation = originName;
			else { 
				if (originNames.containsKey(departureStationId)) {
					Log.v("KEY", "contained");
					departureStation = originNames.get(departureStationId);
				}	
			} 

			String arrivalStationId = currentSegment.getArrivalStation();
			String arrivalStation = null;
			if (arrivalStationId.equals(destinationId))
				arrivalStation = destinationName;
			else {
				if (destinationNames.containsKey(arrivalStationId)) {
					Log.v("KEY", "contained");
					arrivalStation = destinationNames.get(arrivalStationId);
				}
				
			}
			
			String equipment = currentSegment.getEquipment();
			String trainNumber = currentSegment.getTrainNumber();
			
			//TIME
			segmentDepartureTime.setText(TimeHelper.getReadableDateTime(departureTime));
			segmentArrivalTime.setText(TimeHelper.getReadableDateTime(arrivalTime));
			
			//STATIONS
			segmentDepartureStation.setText(departureStation);
			segmentArrivalStation.setText(arrivalStation);
			
			//COMPANY LOGO
			String companyName = TrainCompanies.find(currentSegment.getOperatingCompany()); //using the enum, i mapped all the codes as sncf for example
			if (companyName != null) {
				if (companyName.equals(TrainCompanies.SNCF.toString()))
					segmentCompanyLogo.setBackgroundResource(R.drawable.sn_logo);
				else if(companyName.equals(TrainCompanies.TRENITALIA.toString()))
					segmentCompanyLogo.setBackgroundResource(R.drawable.tre_logo);
				else if(companyName.equals(TrainCompanies.CANADIANSRAILS.toString()))
					segmentCompanyLogo.setBackgroundResource(R.drawable.via_logo);			
			}
			
			
			//EQUIPMENT
			String equipmentText = "";
			try {
				equipmentText = Equipment.valueOf(equipment).toString();
			}
			catch(IllegalArgumentException e) {
				equipmentText = equipment;
			}
			
			segmentEquipment.setText( equipmentText + " N�" + trainNumber);
			
			//FLEXIBILITY
			String flexibilityString = FlexibilityHelper.codeToString(currentSegment.getPricing().get(0).getQuotingRule().getFlexibility().getCode());
			flexibility.setText(flexibilityString);
			
		}
	
		
		return v;
	}
}
