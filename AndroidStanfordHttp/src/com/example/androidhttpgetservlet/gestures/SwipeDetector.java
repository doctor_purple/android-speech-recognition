package com.example.androidhttpgetservlet.gestures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.example.androidhttpgetservlet.interfaces.OnDeleteItemListener;
import com.example.androidhttpgetservlet.models.PendingList;

/**
 * Reports the movements through the Action Object. In the onTouch, there is a check to understand if a swipe movement is detected
 * @author dbattaglino
 *
 */

public class SwipeDetector implements OnTouchListener {
	
	private int SHORT_ANIMATION_DURATION = 300;
	private float downX, upX, slop;
	private boolean mSwipeDetected;
	private final List<PendingList> mPendingList = new ArrayList<PendingList>(); 
	
	//own object
	private ListView listView;
	private OnDeleteItemListener deleteItemListener;
	private int mViewWidth = 1;
	private boolean mPaused;
	private View selectedView;
	private int selectedPosition;
	private boolean collapseRight, collapse;
	private VelocityTracker mVelocityTracker;
	private int mMinFlingVelocity;
	private int mMaxFlingVelocity;
	
	
	public SwipeDetector(ListView listView, OnDeleteItemListener deleteItemListener) {
		ViewConfiguration vc = ViewConfiguration.get(listView.getContext());
		slop = vc.getScaledTouchSlop();
		mMinFlingVelocity = vc.getScaledMinimumFlingVelocity() * 20;
		mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
		this.listView = listView;
		this.deleteItemListener = deleteItemListener;		
		
	}
	
	
	
	//variable for disabling the scroll if is detecting the swiping
	public void setEnabled(boolean enabled) {
		mPaused = !enabled;
	}
	
	public boolean swipeDetected() {
		return mSwipeDetected;
	}
	
	/*
	 * listener on the scroll gesture. When it detects a scrolling, set the flag mPaused to true in order to 
	 * disable the swiping gesture
	 */
	public AbsListView.OnScrollListener makeScrollListener() {
		return new AbsListView.OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				setEnabled(scrollState != OnScrollListener.SCROLL_STATE_TOUCH_SCROLL);
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
			}
		};
	}
	
	
	/*
	 * All the controls and the logic for the swipe gesture recognition are implemented here
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		//take the widht of the list view
		if (mViewWidth < 2) {
			mViewWidth = listView.getWidth(); //width of the list view
		}
		

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN: //finger down on the screen 
			if (mPaused)
				return false;
			
			//find the child touched (rectangle hit test)
			Rect rect = new Rect();
			int itemCount = listView.getCount();
			int[] listViewCoords = new int[2];
			listView.getLocationOnScreen(listViewCoords);
			int x = (int) event.getRawX() - listViewCoords[0];
			int y = (int) event.getRawY() - listViewCoords[1];
			View itemView;
			for(int i=0; i < itemCount; i++) {
				itemView = listView.getChildAt(i);
				if (itemView != null) {
					itemView.getHitRect(rect); //hit test with rectangle
					if (rect.contains(x, y)) {
						selectedView = itemView;
						selectedPosition = i;
						break;
					}
				}		
			}
			
			if (selectedView != null) {
				downX = event.getX(); //get position on x axis where the finger has clicked the first time
                if (deleteItemListener.canDelete(selectedPosition)) {
                    mVelocityTracker = VelocityTracker.obtain(); //obtain the tracking the velocity of the finger, adding the event of the finger
                    mVelocityTracker.addMovement(event);
                } else {
                    selectedView = null;
                }
			}

			return false; //allows other events like Click to be processed
		
		case MotionEvent.ACTION_UP:
			if (mVelocityTracker == null) //if there is not velocity, break the switch
				break;
			
			//variable for the finger up
			upX = event.getX();	
			float deltaX = upX - downX;
			mVelocityTracker.addMovement(event);
			mVelocityTracker.computeCurrentVelocity(1000);
			float velocityX = mVelocityTracker.getXVelocity();
            float absVelocityX = Math.abs(velocityX);
            float absVelocityY = Math.abs(mVelocityTracker.getYVelocity());
			collapse = false;
			collapseRight = false;
			
			//horizontal swipe detction
			if (Math.abs(deltaX) > mViewWidth / 2) {
				collapse = true;
				collapseRight = deltaX > 0;
			}
			else if (mMinFlingVelocity <= absVelocityX && absVelocityX <= mMaxFlingVelocity
                    && absVelocityY < absVelocityX) { //check the velocity of the entire gesture, basing on that I set the flags to collapse 
				collapse = (velocityX < 0) == (deltaX < 0);
				collapseRight = mVelocityTracker.getXVelocity() > 0; 
			}
			
			if (collapse) {
				if (selectedView != null) {
					final View mSelectedView = selectedView; //the view is set to null before the end of animation
					final int mSelectedPosition = selectedPosition;
					selectedView.animate()
					 .translationX(collapseRight ? mViewWidth : - mViewWidth)
					 .alpha(0.4f)
					 .setDuration(SHORT_ANIMATION_DURATION)
					 .setListener(new AnimatorListenerAdapter() {
						
						
						@Override
						public void onAnimationEnd(Animator animation) {
							performDelete(mSelectedView, mSelectedPosition);
							
						}
						
					});
				}
					
			}
			else {
				selectedView.animate()
									 .translationX(0)
									 .alpha(1)
									 .setDuration(SHORT_ANIMATION_DURATION)
									 .setListener(null);
			}
			
			//reset the velocity for the next interaction
			mVelocityTracker.recycle();
			mVelocityTracker = null;
			downX = 0;
			selectedView = null;
			selectedPosition = ListView.INVALID_POSITION;
			mSwipeDetected = false;
			break;
			
		
		case MotionEvent.ACTION_MOVE:
			if (mVelocityTracker == null || mPaused) //mPaused means the user is scrolling (paused from the gesture of swiping)
				break;
			
			mVelocityTracker.addMovement(event);
			float deltaXMove = event.getX() - downX;
			if (Math.abs(deltaXMove) > slop) {
				mSwipeDetected = true;
				listView.requestDisallowInterceptTouchEvent(true); //the child does't want that an event will be intercepted by his parent
				//cancel list view events in this case
				MotionEvent cancelEvent = MotionEvent.obtain(event);
				cancelEvent.setAction(MotionEvent.ACTION_CANCEL | (event.getActionIndex()
                        << MotionEvent.ACTION_POINTER_INDEX_SHIFT));
				listView.onTouchEvent(cancelEvent);
				cancelEvent.recycle();
			}
			
			if (mSwipeDetected) {
				selectedView.setTranslationX(deltaXMove);
				selectedView.setAlpha(Math.max(0.4f, 
									  Math.min(1f, 1f - 2f * Math.abs(deltaXMove / mViewWidth)))); //alpha depending on the position of delta x during the moving
					
				return true;
			}
		break;
		
		}
		return false;	
	}


	/*
	 * Performs deletion
	 */
	private void performDelete(final View selectedView, final int selectedPosition) {
		Log.v("position to delete", "" + selectedPosition);
		final LayoutParams lp = selectedView.getLayoutParams(); //params of the list view layout
		final int originalHeight = selectedView.getHeight(); //original height
		
		ValueAnimator listAnimator = ValueAnimator.ofInt(originalHeight, 1).setDuration(SHORT_ANIMATION_DURATION);
		listAnimator.addListener(new AnimatorListenerAdapter() {

			@Override
			public void onAnimationEnd(Animator animation) {
				
				
				Collections.sort(mPendingList);
				int[] deletePositions = new int[mPendingList.size()]; //the position for each view left
				for (int i=mPendingList.size() -1; i>=0; i--) {
					deletePositions[i] = mPendingList.get(i).position;
				}
				deleteItemListener.onDelete(listView, deletePositions); //list view and the position of the elements to delete
				
				LayoutParams lp;
				for (PendingList pendingDeletion: mPendingList) {
					//reset the view
					pendingDeletion.view.setAlpha(1f);
					pendingDeletion.view.setTranslationX(0);
					lp = pendingDeletion.view.getLayoutParams();
					lp.height = originalHeight;
					pendingDeletion.view.setLayoutParams(lp);
				}
				
				mPendingList.clear();
				
			}	
		});
		
		listAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			
			@Override
			public void onAnimationUpdate(ValueAnimator valueAnimator) {
				lp.height = (Integer) valueAnimator.getAnimatedValue();
				selectedView.setLayoutParams(lp);
				
			}
		});
		
		mPendingList.add(new PendingList(selectedPosition, selectedView));
		listAnimator.start();
	}


}
