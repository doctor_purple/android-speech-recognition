package com.example.androidhttpgetservlet.enums;

import java.util.Arrays;
import java.util.List;

public enum TrainCompanies {
	
	SNCF("SNCF", "SN", "SNF", "FFR"),
	SWP("Sweedish Rails"),
	TRENITALIA("TRE, TR"),
	TR("Trenitalia"),
	DBA("Deutsche Bahn"),
	CANADIANSRAILS("QR");
	
	private final List<String> values;
	
	TrainCompanies(String ...values) {
		this.values = Arrays.asList(values);
	}
	
	public List<String> getvalues() {
		return values;
	}
	
	public static String find(String name) {
		for (TrainCompanies company: TrainCompanies.values()) {
			if (company.getvalues().contains(name)) {
				return company.name();
			}
		}
		return null;
	}
}
