package com.example.androidhttpgetservlet.enums;

import java.util.Arrays;
import java.util.List;

//commands of keywords
public enum Commands {
	CONFIRM("confirm", "yes", "okay", "ok"), 
	CHEAPEST("cheapest", "cheap", "inexpensive", "economic", "low cost", "less expensive"),
	CLOSEST("closest", "first train to leave"), 
	CLOSEST_ARRIVAL("closest to arrive"),
	SHORTEST("shortest", "short");
	
private final List<String> values;
	
	Commands(String ...values) {
		this.values = Arrays.asList(values);
	}
	
	public List<String> getvalues() {
		return values;
	}
	
	public static String find(String sentence) {
		for (Commands confirmationCommand: Commands.values()) {
			for (String keyword: confirmationCommand.getvalues()) {
				if (sentence.contains(keyword))
					return confirmationCommand.name();
			}	
		}
		return null;
	}
}
