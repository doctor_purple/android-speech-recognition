package com.example.androidhttpgetservlet.enums;

public enum Equipment {
	
    TER("Regional Express train"),
    TGA("TGV Atlantique"),
    TGD("TGV Duplex"),
    TGH("Thalys"),
    TGI("TGV Inter R�seaux"),
    TGN("TGV Nord"),
    TGE("TGV Est"),
    TGS("TGV Sud-Est"),
    TGT("Eurostar"),
    THT("Trainh�tel Elipsos"),
    TJC("Corail Teoz"),
    TNC("Train de Nuit Confort"),
    TRN("Train"),
    TRC("Nigth Comfort regional train"),
    TIR("Corail Intercit�s"),
    CIC("Corail Intercit�s"),
    XDE("X2000"),
    ICE("InterCity"),
    NTZ("Nachtzug"),
    CNL("City Nigth Line"),
    ARN("Night Artesia"),
    ARJ("Day Artesia"),
    TGM("Day Artesia"),
    NAV("Shuttle"),
    CAR("Bus"),
    TGL("Lyria"),
    TGJ("TGV Lyria Duplex"),
    TGP("TGV Est toward Munich"),
    TGG("TGV Est toward Frankf�rt"),
    TNS("Nigth Service train"),
    TGU("Nigth TGV"),
    TGX("Duplex Nigth TGV"),
    AVE("Spanish train AVE"),
    ETR("Pendolino"),
    BAN("Suburbans train"),
    RER("RER Train"),
    REA("RER A"),
    REB("RER B"),
    REC("RER C"),
    RED("RER D"),
    SAE("Car express service"),
    TAA("Car accompagnated Train"),
    TAC("Car with Berth Train"),
    TAJ("Car day train"),
    TAS("Car specialized train"),
    TGB("Bruxelles Province"),
    TGO("Duplex Bruxelles Province"),
    TGW("Gen�ve M�diterran�e"),
    TGC("Duplex Gen�ve M�diterran�e"),
    TGK("France Italie"),
    TGF("TGV Duplex"),
    RID("Train Riviera"),
    BWE("Allemagne pologne"),
    CIS("En italie"),
    EAV("En italie"),
    ICP("En italie"),
    TBZ("En italie"),
    ESI("France Italie"),
    ICT("France Italie"),
    PME("France Italie"),
    TGQ("TGV FAMILY"),
    TGY("TGV FAMILY DUPLEX"),
    TJA("R�serv� Corail de jour 1"),
    TJB("R�serv� Corail de jour 2"),
    TNA("R�serv� Corail Lun�a 1"),
    TNB("R�serv� Corail Lun�a 2"),
    TGZ("Equipement exp�rimentation"),
    TID("Train inter-cit�s duplex"),
    TIB("Train inter-cit�s UFR"),
    TIV("Train inter-cit�s de r�serve"),
    RLJ("Railjet"),
    BET("Bus"),
    ECB("EuroCity Brenner"),
    FA("Freccia Argento"),
    FB("Freccia Bianca"),
    FR("Freccia Rossa");
    
    
    
    private final String equipment;
    private Equipment(String equipment) {
    	this.equipment = equipment;
    }
	
    
    public String toString() {
    	return getEquipment();
    }
    
    public String getEquipment() {
    	return this.equipment;
    }
} 

