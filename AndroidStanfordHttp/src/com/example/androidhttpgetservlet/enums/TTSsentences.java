package com.example.androidhttpgetservlet.enums;


public enum TTSsentences {
	/*
	 * These enum is for the sentences used by the tts system
	 */

	DATE("The date is missing: I set the date of today"),
	DESTINATION("Destination missing, please repeat"),
	CONFIRM("You want to confirm?"),
	SPEECH_HINT("Press the microphone and speak!");
	
	
	//methods to be called from other classes
	private final String sentence;
	private TTSsentences(String sentence) {	
		this.sentence = sentence;
	}
	
	public String toString() {
		return getSentence();
	}
	
	protected String getSentence() {
		return this.sentence;
	}
	
	public static TTSsentences get( String value ) {
		for( TTSsentences v : values() ) {
			if( value.equals( v.getSentence())) {
				return v;
		    }
		}
		throw new IllegalArgumentException( "Unknown label of speech: '" + value + "'." );  
	}	
}
