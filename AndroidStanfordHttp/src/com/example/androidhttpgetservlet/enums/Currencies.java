package com.example.androidhttpgetservlet.enums;

import java.util.Arrays;
import java.util.List;

public enum Currencies {
	
	$("CAD"), 
	�("Eur");
	
	private final List<String> values;
	
	Currencies(String ...values) {
		this.values = Arrays.asList(values);
	}
	
	public List<String> getvalues() {
		return values;
	}
	
	public static String find(String name) {
		for (Currencies currency: Currencies.values()) {
			for (String keyword: currency.getvalues()) {
				if (keyword.equalsIgnoreCase(name)) {
					return currency.name();
				}	
			}
		}
		return null;
	}
}
