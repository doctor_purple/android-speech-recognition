package com.example.androidhttpgetservlet;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;


import com.example.androidhttpgetservlet.fragments.FieldsFragment;
import com.example.androidhttpgetservlet.fragments.SpeechFragment;
import com.example.androidhttpgetservlet.interfaces.OnClickSpeechListener;



public class MainActivity extends FragmentActivity implements OnClickSpeechListener, LocationListener {
	
	
	//Connection manager
	ConnectivityManager connectivityManager;
	public double latitude, longitude; 
	
	//Fragments
	FieldsFragment fieldsFragment, newFieldsFragment;
	
	
	/*
	 * Called the first time the activity is created	
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE); //connection manager

		if (isConnected()) {
			//get the latitude and longitude of the device
			LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			if (locationManager != null) {
				Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				if (location != null) {
					
					latitude = location.getLatitude();
					longitude = location.getLongitude();
					Log.v("COORDINATES", latitude + "---->" + longitude);
				}
				else {
					locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
				}
			}
			else
				Log.v("LOCATION MANAGER", "location manager is null");
		}
		
		
		//check whether the activity is using the layout version with the fragment_container FrameLayout. 
		//If so, the first fragment is called 
		if (findViewById(R.id.fragment_container) != null) {
			
			
			//for avoiding the overlapping fragment
			if (savedInstanceState != null) {
				return;
			}
			
			//Creates an instance of SpeechFragment, and passes the information in the bundle
			SpeechFragment speechFragment = new SpeechFragment();
			speechFragment.setArguments(getIntent().getExtras());
			
			//Adds the fragments to the 'fragment_container' layout
			getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, speechFragment).commit();
		}
	}
	


	@Override
	protected void onRestart() {
		super.onRestart();
		//capture the results fragment from the activity layout
		fieldsFragment = (FieldsFragment)
				getSupportFragmentManager().findFragmentById(R.id.results_fragment);
		
		if (fieldsFragment != null) {
			fieldsFragment.resetVariables();
			fieldsFragment.resetViewElements();
		}
	}



	/*
	 * since it is not possibile to call fragments from fragment, we need to pass through the MainActivty
	 */
	public void startResultsFragment(Bundle bundle) {
		
		//capture the results fragment from the activity layout
		fieldsFragment = (FieldsFragment)
				getSupportFragmentManager().findFragmentById(R.id.results_fragment);
		
		if (fieldsFragment != null) {
			//we are in the case of a two-pane layout
			fieldsFragment.mapDataToEditText(bundle);
			
			
		}
		else {
			//if the fragment is null, we are in the case of one-pane, so I have to create again the fragment from the scratch
			newFieldsFragment = new FieldsFragment();
			newFieldsFragment.setArguments(bundle);
			getSupportFragmentManager().beginTransaction()
				.replace(R.id.fragment_container, newFieldsFragment)
				.addToBackStack(null)
				.commit();		
		}
	}

	
	/*
	 * Connection available
	 */
	private boolean isConnected() {
		NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
		if (ni == null) 
			return false;
		else return ni.isConnected();
	}
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------LOCATION LISTENER METHODS-----------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	
	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLatitude();
		}	
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

	


}
