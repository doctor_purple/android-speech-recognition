package com.example.androidhttpgetservlet.threads;

import android.os.AsyncTask;

import com.example.androidhttpgetservlet.helpers.JSONHelper;
import com.example.androidhttpgetservlet.models.City;

public class ClosestCityNameTask extends AsyncTask<String, Void, City> {

	@Override
	protected City doInBackground(String... params) {
		String url = params[0];
		String latitude = params[1];
		String longitude = params[2];
		
		try {
			return JSONHelper.getCityFromCoordinates(url, latitude, longitude);
		}
		catch (Exception e) {
			return null;
		}
	}
}
