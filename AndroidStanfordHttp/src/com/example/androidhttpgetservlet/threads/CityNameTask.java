package com.example.androidhttpgetservlet.threads;


import java.util.HashMap;

import android.os.AsyncTask;
import com.example.androidhttpgetservlet.helpers.JSONHelper;


/**
 * 
 * @author dbattaglino
 * Class that performs the async task for getting all the cities given a name
 */
public class CityNameTask extends AsyncTask<String, Void, HashMap<String, String>> {
	HashMap<String, String> cityNames;

	@Override
	protected HashMap<String, String>  doInBackground(String... params) {
		String url = params[0];
		String field = params[1];
		
		
		try {
			return JSONHelper.getNameIdCityJSON(url, field);
		} 
		catch (Exception e) {
			return null;
		}
	}
	
	
	@Override
	protected void onPostExecute(HashMap<String, String> cityNames) {
		this.cityNames = cityNames;
	}	
}
