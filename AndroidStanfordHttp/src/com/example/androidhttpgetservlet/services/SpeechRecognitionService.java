package com.example.androidhttpgetservlet.services;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.example.androidhttpgetservlet.helpers.JSONHelper;

public class SpeechRecognitionService extends Service {
	protected AudioManager audioManager;
	protected SpeechRecognizer speechRecognizer;
	protected Intent speechRecognizerIntent;
	protected final Messenger serverMessenger = new Messenger(new IncomingHandler(this));
	private Intent intent;
	
	boolean isListening;
	volatile boolean isCountDown;
	
	private ArrayList<String> googleSpeech_results;
	
	public static final int MSG_RECOGNIZER_START_LISTENING = 1;
	public static final int MSG_RECOGNIZER_CANCEL = 2;
	private static final String TAG = "Speech Service";
	public static final String BROADCAST_ACTION = "broadcast_action";
	

	@Override
	public IBinder onBind(Intent intent) { 
		Log.d(TAG, "onBind");
		return serverMessenger.getBinder();
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
		speechRecognizer.setRecognitionListener(new SpeechRecognitionListener());
		speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		//this set the speech model to be recognized
		speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, JSONHelper.LOCALE_LANGUAGE);
		speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, JSONHelper.LOCALE_LANGUAGE);
		speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Confirm your choice?");
		speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
		speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);
		speechRecognizerIntent.putExtra("calling_package", this.getPackageName());
		
		//intent to send back to information to the activity
		intent = new Intent(BROADCAST_ACTION);
	}
	
	@Override
	public void onDestroy() {
		
		super.onDestroy();
		if (speechRecognizer != null) {
			speechRecognizer.destroy();
		}
	}

	protected static class IncomingHandler extends Handler{
		private WeakReference<SpeechRecognitionService> target; //reference that does not protect the object from collection by a garbage collector
		IncomingHandler(SpeechRecognitionService speechTarget){
			target = new WeakReference<SpeechRecognitionService>(speechTarget);
			
			
		}
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			
			final SpeechRecognitionService speechService = target.get();
			switch (msg.what) {
			case MSG_RECOGNIZER_START_LISTENING:
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					//turn off beep sound
					speechService.audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
				}
				if (!speechService.isListening) {
					speechService.speechRecognizer.startListening(speechService.speechRecognizerIntent);
					speechService.isListening = true;
					//Log.d(TAG, "speech service started");
				}
				break;
			case MSG_RECOGNIZER_CANCEL:
				speechService.speechRecognizer.cancel();
				speechService.isListening = false;
				//Log.d(TAG, "speech service canceled");
				break;
			}
		}
	}
	
	protected class SpeechRecognitionListener implements RecognitionListener{

		@Override
		public void onBeginningOfSpeech() {
			Log.d(TAG, "onBeginingOfSpeech");
			
		}

		@Override
		public void onBufferReceived(byte[] arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onEndOfSpeech() {
			// TODO Auto-generated method stub
			//Log.d(TAG, "onEndOfSpeech");
		}

		@Override
		public void onError(int arg0) {
			isListening = false;
			Message message= Message.obtain(null, MSG_RECOGNIZER_START_LISTENING);
			try {
				serverMessenger.send(message);
			}
			catch (RemoteException e) {
				
			}		
		}

		@Override
		public void onEvent(int arg0, Bundle arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onPartialResults(Bundle arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onReadyForSpeech(Bundle arg0) {
			//Log.d(TAG, "onReadyForSpeech"); 
		}

		@Override
		public void onResults(Bundle data) {
			if (data != null) {
				googleSpeech_results = data.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
				//Log.d(TAG, googleSpeech_results.get(0));
				intent.putExtra(JSONHelper.TAG_TEXT, googleSpeech_results.get(0));
				sendBroadcast(intent);
			}
			Message message = Message.obtain(null, MSG_RECOGNIZER_CANCEL);
			try {
				serverMessenger.send(message);
				message = Message.obtain(null, MSG_RECOGNIZER_START_LISTENING);
				serverMessenger.send(message);
			}catch (RemoteException e) {}
		}

		@Override
		public void onRmsChanged(float arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
