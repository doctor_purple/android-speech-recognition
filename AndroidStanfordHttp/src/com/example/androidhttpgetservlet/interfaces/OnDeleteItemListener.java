package com.example.androidhttpgetservlet.interfaces;

import android.widget.ListView;


/*
 * called to know which position has to be deleted from the list
 */
public interface OnDeleteItemListener {
	boolean canDelete(int position);
	void onDelete(ListView listView, int[] positions); //TODO: think about the ordering
	
}
