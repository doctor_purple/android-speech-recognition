package com.example.androidhttpgetservlet.interfaces;
import android.os.Bundle;

public interface OnClickSpeechListener {
	public void startResultsFragment(Bundle bundle);
}
