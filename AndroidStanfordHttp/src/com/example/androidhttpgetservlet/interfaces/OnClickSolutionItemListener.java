package com.example.androidhttpgetservlet.interfaces;

public interface OnClickSolutionItemListener {
	public void onSolutionSelected(int position);
}
