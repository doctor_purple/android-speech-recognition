package com.example.androidhttpgetservlet.models;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import android.util.Log;

import com.example.androidhttpgetservlet.helpers.TimeHelper;



/**
 * Created by dbattaglino on 29/08/13.
 * This class is a mapper to parse automatically the trips solution in json.
 * It implements also several comparators, used by the filters helper class
 */
public class Solution implements Serializable {

	private static final long serialVersionUID = -438619084705946494L;
	private String originLocation;
	private String destinationLocation;
	private String error_code;
	public String getError_code() {return error_code;}
	public void setError_code(String error_code) {this.error_code = error_code;}
	public String getError_message() {return error_message;}
	public void setError_message(String error_message) {this.error_message = error_message;}
	private String error_message;
	private ArrayList<OriginDestinationOptions> OriginDestinationOptions;
	public String getOriginLocation() {return originLocation;}
	public String getDestinationLocation() {return destinationLocation; }
	public ArrayList<OriginDestinationOptions> getOriginDestinationOptions() {return OriginDestinationOptions; }
	
    public static class OriginDestinationOptions implements Serializable, Comparable<OriginDestinationOptions> {
    	/**
		 * 
		 */
		private static final long serialVersionUID = -2275612743620217978L;
		public ArrayList<JourneySegments> JourneySegments;
        public ArrayList<Pricings> Pricings;
        public ArrayList<JourneySegments> getJourneySegments() { return JourneySegments; }
        public ArrayList<Pricings> getPricings() { return Pricings; }
        
     
    	
    
    	//PRIVATE CLASSES
        public static class JourneySegments implements Serializable {
            /**
			 * 
			 */
			private static final long serialVersionUID = 1167514924479032862L;
			public String departureStation;
            public String arrivalStation;
        	public String departureDateTime;
            public String arrivalDateTime;
            public String transportationCode;
            public String trainNumber;
            public String marketingCompany;
            public String operatingCompany;
            public String equipment;
            public String servicesOnBoard;
            private ArrayList<Pricing> Pricing;
            
            
            
            public String getDepartureStation() {return departureStation;}
            public String getArrivalStation() {return arrivalStation;}
            public String getTransportationCode() { return transportationCode; }
            public String getArrivalDateTime() { return arrivalDateTime; }
            public String getDepartureDateTime() { return departureDateTime; }
            public String getTrainNumber() { return trainNumber; }
            public String getMarketingCompany () { return marketingCompany; }
            public String getOperatingCompany () { return operatingCompany; }
            public String getEquipment() { return equipment; }
            public String servicesOnBoard() { return servicesOnBoard; }
            public ArrayList<Pricing> getPricing() { return Pricing; }
            
            
            //pricing for one segment
            public static class Pricing implements Serializable {
            	/**
				 * 
				 */
				private static final long serialVersionUID = -3911983872503997205L;
				private QuotingRule QuotingRule;
            	public QuotingRule getQuotingRule() { return QuotingRule; }
            	
            	
            	public static class QuotingRule implements Serializable {
            		/**
					 * 
					 */
					private static final long serialVersionUID = 1095032825023446837L;
					private flexibility flexibility;
            		public flexibility getFlexibility() { return flexibility; }
            		
            		
            		public static class flexibility implements Serializable {
            			/**
						 * 
						 */
						private static final long serialVersionUID = 6121143093112538432L;
						private String code;
            			private String description;
            			
            			public String getCode() { return code; }
            			public String getDescription() { return description; }
            			
            		}
            	}
            }
            
            
        }
        public static class Pricings implements Serializable {
           
            /**
			 * 
			 */
			private static final long serialVersionUID = 1522774861157412632L;
			private Accomodation Accomodation;
            private AmounterAfterTax AmounterAfterTax;
            public Accomodation getAccomodation() { return Accomodation; }
            public AmounterAfterTax getAmounterAfterTax() { return AmounterAfterTax; }

            public static class Accomodation implements Serializable {
                /**
				 * 
				 */
				private static final long serialVersionUID = -7441748002777963999L;
				private String serviceClass;
                public String getServiceClass() { return serviceClass; }
            }

            public static class AmounterAfterTax implements Serializable {
                /**
				 * 
				 */
				private static final long serialVersionUID = -203383418591876310L;
				private String amount;
                private String currency;
                public String getAmount() { return amount; }
                public String getCurrency() { return currency; }
            }
        }
        
        
        //THE DEFAULT COMPARE IS ON THE PRICE
		@Override
		public int compareTo(OriginDestinationOptions another) {
			int thisAmount = Integer.valueOf(this.getPricings().get(0).getAmounterAfterTax().getAmount());
			int anotherAmount = Integer.valueOf(another.getPricings().get(0).getAmounterAfterTax().getAmount());
		
			return thisAmount - anotherAmount;
		}
		
		 //COMPARATOR OF DURATION
    	public static class DurationComparator implements Comparator<OriginDestinationOptions> {
    		 @Override
    			public int compare(OriginDestinationOptions od1, OriginDestinationOptions od2) {
    				int duration1 = TimeHelper.computeDurationInMinutes(od1.getJourneySegments().get(0).getDepartureDateTime(), 
    														   			od1.getJourneySegments().get(0).getArrivalDateTime());
    				int duration2 = TimeHelper.computeDurationInMinutes(od2.getJourneySegments().get(0).getDepartureDateTime(), 
    						   											od2.getJourneySegments().get(0).getArrivalDateTime());
    				
    				return duration1 - duration2;
    			}
    	}
    	
    	//COMPARATOR OF THE DEPARTURE TIME
    	public static class TimeComparator implements Comparator<OriginDestinationOptions> {

    		@Override
    		public int compare(OriginDestinationOptions od1, OriginDestinationOptions od2) {
    			Date date1 = null;
    			Date date2 = null;
    			
    			try {
    				date1 = TimeHelper.getDateFromString(od1.getJourneySegments().get(0).getDepartureDateTime());
    				date2 = TimeHelper.getDateFromString(od2.getJourneySegments().get(0).getDepartureDateTime());
    			} catch (ParseException e) {
    				Log.e("Date Parsing", "error in parsing the date");
    			}
    			
    			if (date1 != null && date2 != null) {
    				long diff1 = TimeHelper.computeDifference(new Date(), date1);
    				long diff2 = TimeHelper.computeDifference(new Date(), date2);
    				
    				//compare return statement 
    				if (diff1 > diff2)
    					return 1;
    				else if (diff1 < diff2)
    					return -1;
    				else 
    					return 0;
    			}
    			else return 0;
    		}
    	}
    	
    	//COMPARATOR OF THE ARRIVAL TIME
    	public static class ArrivalTimeComparator implements Comparator<OriginDestinationOptions> {

    		@Override
    		public int compare(OriginDestinationOptions od1, OriginDestinationOptions od2) {
    			Date date1 = null;
    			Date date2 = null;
    			
    			try {
    				date1 = TimeHelper.getDateFromString(od1.getJourneySegments().get(0).getArrivalDateTime());
    				date2 = TimeHelper.getDateFromString(od2.getJourneySegments().get(0).getArrivalDateTime());
    			} catch (ParseException e) {
    				Log.e("Date Parsing", "error in parsing the date");
    			}
    			
    			if (date1 != null && date2 != null) {
    				long diff1 = TimeHelper.computeDifference(new Date(), date1);
    				long diff2 = TimeHelper.computeDifference(new Date(), date2);
    				
    				//compare return statement 
    				if (diff1 > diff2)
    					return 1;
    				else if (diff1 < diff2)
    					return -1;
    				else 
    					return 0;
    			}
    			else return 0;
    		}   
    	}
    }
}