package com.example.androidhttpgetservlet.models;

import android.view.View;

public class PendingList implements Comparable<PendingList> {

	public int position;
	public View view;
	
	
	public PendingList (int position, View view) {
		this.position = position;
		this.view = view;
	}
	
	
	public int getPosition () {
		return position;
	}
	
	@Override
	public int compareTo(PendingList another) {
		
		return another.position - position;
	}
	
}
