package com.example.androidhttpgetservlet.models;

public class City {
	private String crbCode, cityName;
	
	
	public City(String crbCode, String cityName) {
		this.crbCode = crbCode;
		this.cityName = cityName;
	}
	
	public String getcrbCode() {
		return crbCode;
	}
	
	public String getCityName() {
		return cityName;
	}
}
